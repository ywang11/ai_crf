import xarray as xr
from utils.settings import *
import os
import pandas as pd
import numpy as np


lat = np.arange(-55.75, 69.76, 0.5)
lat0 = np.arange(-60, 71, 5)

labels0 = ['North American', 'South American', 'Europe', 'Northern Africa',
           'Southern Africa', 'Eastern Asia', 'Oceania']
lat1 = [20, -25, 37, 9, -37, 20, -45]
lat2 = [58,  5, 60,  27, -5, 55, -10]
lon1 = [-127, -83, 10, -18, -10, 90, 112]
lon2 = [ -51, -33, 52,  36,  70, 147, 155]


hr = xr.open_dataset(os.path.join(path_data, 'elm_half_degree_grid_negLon.nc'))
area = hr['area'].values.copy()
hr.close()

lon2d, lat2d = np.meshgrid(hr.lon.values, hr.lat.values)


########
# 0.5 degree division
########
lat_area = pd.DataFrame(data = np.nan, 
                        index = ['%.2f' % x for x in lat],
                        columns = ['global', 'SH', 'NH'] + labels0)
for x in lat:
    lat_area.loc['%.2f' % x, 'global'] = \
        np.nansum(area[np.abs(hr.lat.values - x) < 1e-3, :])

for x in lat[lat < 0]:
    lat_area.loc['%.2f' % x, 'SH'] = \
        np.nansum(area[np.abs(hr.lat.values - x) < 1e-3, :])

for x in lat[lat > 0]:
    lat_area.loc['%.2f' % x, 'NH'] = \
        np.nansum(area[np.abs(hr.lat.values - x) < 1e-3, :])


for i in range(len(labels0)):
    area_x = np.where((lat2d >= lat1[i]) & (lat2d <= lat2[i]) & \
                      (lon2d >= lon1[i]) & (lon2d <= lon2[i]), 
                      area, np.nan)
    for x in lat:
        lat_area.loc['%.2f' % x, labels0[i]] = \
            np.nansum(area_x[np.abs(hr.lat.values - x) < 1e-3, :])
lat_area[lat_area < 1e-3] = np.nan
lat_area.to_csv(os.path.join(path_out, 'bylat_area_0.5.csv'))


########
# 5 degree division
########
lat_area0 = pd.DataFrame(data = np.nan, 
                         index = ['%.2f' % (x+2.5) for x in lat0[:-1]],
                         columns = ['global', 'SH', 'NH'] + labels0)
for x in lat0[:-1]:
    lat_area0.loc['%.2f' % (x+2.5), 'global'] = \
        np.nansum(area[(hr.lat.values >= x) & (hr.lat.values < (x+5)), :])

for x in lat0[lat0 < 0][:-1]:
    lat_area0.loc['%.2f' % (x+2.5), 'SH'] = \
        np.nansum(area[(hr.lat.values >= x) & (hr.lat.values < (x+5)), :])

for x in lat0[lat0 > 0][:-1]:
    lat_area0.loc['%.2f' % (x+2.5), 'NH'] = \
        np.nansum(area[(hr.lat.values >= x) & (hr.lat.values < (x+5)), :])

for i in range(len(labels0)):
    area_x = np.where((lat2d >= lat1[i]) & (lat2d <= lat2[i]) & \
                      (lon2d >= lon1[i]) & (lon2d <= lon2[i]), 
                      area, np.nan)
    for x in lat0[:-1]:
        lat_area0.loc['%.2f' % (x+2.5), labels0[i]] = \
            np.nansum(area_x[(hr.lat.values >= x) & (hr.lat.values < (x+5)),
                             :])
lat_area0[lat_area0 < 1e-3] = np.nan
lat_area0.to_csv(os.path.join(path_out, 'bylat_area_5.csv'))
