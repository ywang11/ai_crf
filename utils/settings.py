import os
from glob import glob
import numpy as np
import pandas as pd


path_root = os.path.join(os.environ['PROJDIR'], 'AI')
path_data = os.path.join(path_root, 'data')
path_out = os.path.join(path_root, 'output')


def path_out_da(which, r, ensemble = ''):
    # which = ['da1', 'da2']
    # r = ['global', '7region']
    if ensemble == 'all':
        return os.path.join(path_out, which, 'run_1_all_ensemble_members', r)
    elif ensemble == 'one':
        return os.path.join(path_out, which, 'run_2_one_ensemble_member', r)
    elif ensemble == 'large':
        return os.path.join(path_out, which, 'run_3_large_ensemble_members', r)
    elif ensemble == 'limited_all':
        return os.path.join(path_out, which, 'run_4_all_ensemble_members', r)
    elif ensemble == 'limited_large':
        return os.path.join(path_out, which, 'run_5_large_ensemble_members', r)
    elif ensemble == 'uniform_all':
        return os.path.join(path_out, which, 'run_6_all_ensemble_members', r)
    elif ensemble == 'uniform_large':
        return os.path.join(path_out, which, 'run_7_large_ensemble_members', r)
    else:
        return os.path.join(path_out, which, r)


def get_fdict(e, aggr = 'nc', var = '', r = ''):
    if aggr == 'nc':
        if var != 'Arid_234':
            #if 'piControl' in e:
            #    prefix = os.path.join(path_data, 'CMIP6-AI', e, 'Detrend')
            #else:
            prefix = os.path.join(path_data, 'CMIP6-AI', e)
        else:
            prefix = os.path.join(path_out, 'Arid_234', 'CMIP6-AI', e)

        flist = glob(os.path.join(prefix, var.split('_')[0] + '*.nc'))
        mlist = list(np.unique([f.split('_')[-3] for f in flist]))
        fdict = {}
        for m in mlist:
            if m == 'KACE-1-0-G':
                continue
            fdict[m] = glob(os.path.join(prefix, var.split('_')[0] + \
                                         '_*_' + m + '_*.nc'))
    else:
        if aggr == 'lat':
            if (r == 'global') | (r == 'NH') | (r == 'SH'):
                if var != 'Arid_234':
                    prefix = os.path.join(path_data, 'CMIP6-AI', 
                                          e + '-Lat', var)
                else:
                    prefix = os.path.join(path_out, 'Arid_234', 'CMIP6-AI',
                                          e + '-Lat')
            else:
                if var != 'Arid_234':
                    prefix = os.path.join(path_data, '7-Regions', r,
                                          e + '-Lat', var)
                else:
                    prefix = os.path.join(path_out, 'Arid_234', '7-Regions', r,
                                          e + '-Lat')
        else:
            if (r == 'global') | (r == 'NH') | (r == 'SH'):
                if var != 'Arid_234':
                    prefix = os.path.join(path_data, 'CMIP6-AI', e + '-TS',
                                          var)
                else:
                    prefix = os.path.join(path_out, 'Arid_234', 'CMIP6-AI',
                                          e + '-TS')
            else:
                if '-' in e:
                    e = e.split('-')[1]
                if var != 'Arid_234':
                    fdict = glob(os.path.join(path_data, '7-Regions', r,
                                              'AI-TS',
                                              var + '_*_' + e + '*.csv'))
                else:
                    fdict = glob(os.path.join(path_out, 'Arid_234', 
                                              '7-Regions', r, 'AI-TS',
                                              'Arid_*_' + e + '*.csv'))
                return fdict

        fdict = {}
        flist = glob(os.path.join(prefix, '*.csv'))
        mlist = list(np.unique([f.split('_')[-4] for f in flist]))
        if aggr == 'lat':
            if ((r == 'global') | (r == 'NH') | (r == 'SH')):
                if ('Arid' in var) & (e == 'AI-ALL'):
                    mlist = list(np.unique([f.split('_')[-3] for f in flist]))
            else:
                if (e == 'AI-GHG'):
                    mlist = list(np.unique([f.split('_')[-3] for f in flist]))
        for m in mlist:
            if m == 'KACE-1-0-G':
                continue
            fdict[m] = glob(os.path.join(prefix, '*_' + m + '_*.csv'))
    return fdict


def get_mlist(e):
    # (should be the same for aggregations and maps)
    flist = glob(os.path.join(path_data, 'CMIP6-AI', e, '*.nc'))

    mlist = list(np.unique([f.split('_')[-3] for f in flist]))

    mr_dict = {}
    mr_list = []
    for m in mlist:
        if m == 'KACE-1-0-G':
            continue

        mr_dict[m] = []

        rlist = glob(os.path.join(path_data, 'CMIP6-AI', e,
                                  '*_' + m + '_*.nc'))
        for r in rlist:
            mr_dict[m].append(r.split('_')[-2])
            mr_list.append(m + '_' + r.split('_')[-2])
    return mr_dict, mr_list


def get_yr(e):
    # (should be the same for aggregations and maps)

    flist = glob(os.path.join(path_data, 'CMIP6-AI', e + '-Lat', 'AI',
                              '*.csv'))

    temp = flist[0].split('_')[-1].split('-')

    return range(int(temp[0][:4]), int(temp[1][:4])+1)


def get_oname(o, aggr = 'nc', var = '', r = ''):
    #year_actual = range(1901, 2017)
    year_actual = range(1948, 2017)

    if var != 'Arid_234':
        if aggr == 'nc':
            fname = os.path.join(path_out, 'obs_avg', o, var + '_nc.nc')
        elif aggr == 'lat':
            if (r == 'global') | (r == 'NH') | (r == 'SH'):
                fname = os.path.join(path_out, 'obs_avg', o + '-Lat',
                                     var + '_lat.csv')
            else:
                fname = os.path.join(path_out, 'obs_avg', o + '-Lat',
                                     var + '_lat_' + r + '.csv')
        else:
            if r == 'global':
                fname = os.path.join(path_out, 'obs_avg', o + '-TS', 
                                     var + '_global.csv')
            elif (r == 'NH') | (r == 'SH'):
                fname = os.path.join(path_out, 'obs_avg', o + '-TS', 
                                     var + '_hemisphere.csv')
            else:
                fname = os.path.join(path_out, 'obs_avg', o + '-TS', 
                                     var + '_' + aggr + '_' + r + '.csv')
    else:
        if aggr == 'nc':
            fname = os.path.join(path_out, 'Arid_234', 'obs_avg', o, 
                                 var + '_nc.nc')
        elif aggr == 'lat':
            if (r == 'global') | (r == 'NH') | (r == 'SH'):
                fname = os.path.join(path_out, 'Arid_234', 'obs_avg',
                                     o + '-Lat', var + '_lat.csv')
            else:
                fname = os.path.join(path_out, 'Arid_234', 'obs_avg',
                                     o + '-Lat', var + '_lat_' + r + '.csv')
        else:
            if r == 'global':
                fname = os.path.join(path_out, 'Arid_234', 'obs_avg',
                                     o + '-TS', var + '_global.csv')
            elif (r == 'NH') | (r == 'SH'):
                fname = os.path.join(path_out, 'Arid_234', 'obs_avg',
                                     o + '-TS', var + '_hemisphere.csv')
            else:
                fname = os.path.join(path_out, 'Arid_234', 'obs_avg', 
                                     o + '-TS', 
                                     var + '_' + aggr + '_' + r + '.csv')

    return fname, year_actual
