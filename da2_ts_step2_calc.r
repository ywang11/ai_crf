library(methods)
source('ECOF_V1.r')

# MODIFY
remove_bcc = FALSE
remove_cesm_cesmw = FALSE
remove_noresm = FALSE
# 'large', 'all', 'one', 'uniform_large', 'uniform_all', 'limited_large',
# 'limited_all'
ensemble_id = 'limited_all'
double_noise = FALSE # TRUE, FALSE


# Note: elist and olist must have the same order as ./utils/constants.py
#       Must run under the "conda3environ" environment, not "r_env",
#       otherwise the results differ a lot.
elist = c('historical', 'ANT', 'GHG', 'AER', 'NAT', 'No-AER', 'No-GHG',
          'No-AER-GHG')
olist = c('CRU', 'GPCC', 'UDEL', 'PREC', 'GLDAS', '20CR', 'ERA5', 'Avg')
vlist = c('AI')
tslist = c('Global', 'NH', 'SH')
##, 'Eastern Asia', 'Europe', 'North American',
## 'Northern Africa', 'Oceania', 'South American', 'Southern Africa')
##tslist = c('Global', '31-70N', '0-30N', 'SH')
rtslist = c('TS')
tlist = c(5, 10) # c(6, 11)
blist = list(seq(1965, 2014)) # list(seq(1948, 2014))
yclim = seq(1975, 2005) # Provided below! Depends on tlist
signals.list = list(c('historical'), c('ANT'), c('GHG'), c('AER'), c('NAT'),
	            c('No-AER'), c('No-GHG'), c('No-AER-GHG'), c('ANT','NAT'),
		    c('GHG', 'No-GHG', 'NAT'), c('AER', 'No-AER', 'NAT'),
                    c('GHG', 'AER', 'No-AER-GHG', 'NAT'))
if (ensemble_id == 'large'){
    ensemble_path = 'run_3_large_ensemble_members'
} else if (ensemble_id == 'one'){
    ensemble_path = 'run_2_one_ensemble_member'
} else if (ensemble_id == 'all') {
    ensemble_path = 'run_1_all_ensemble_members'
} else if (ensemble_id == 'limited_all'){
    ensemble_path = 'run_4_all_ensemble_members'
} else if (ensemble_id == 'limited_large'){
    ensemble_path = 'run_5_large_ensemble_members'
} else if (ensemble_id == 'uniform_all'){
    ensemble_path = 'run_6_all_ensemble_members'
} else {
    ensemble_path = 'run_7_large_ensemble_members'
}
if (double_noise){
    suffix = 'double_noise'
} else {
    suffix = 'single_noise'
}

# Number of models/ensemble members that are averaged over to get the signal.
get.nsig  <- function(expr, yrbrac){
    if (yrbrac[length(yrbrac)] == 2014){
        if (ensemble_id == 'all'){
            # Achtung! Must change if some models are excluded.
            # Watch the outputs of "da2_ts_step1_create_input_obs_sig.py"
            if (expr == 'historical'){
                nsig = 43
            } else if ((expr == 'ANT') | (expr == 'NAT')){
                nsig = 18
            } else if ((expr == 'GHG') | (expr == 'No-GHG')){
                nsig = 18
            } else if ((expr == 'AER') | (expr == 'No-AER')){
                nsig = 15
            } else if (expr == 'No-AER-GHG'){
                nsig = 15
            }
        } else if (ensemble_id == 'large') {
            if (expr == 'historical'){
                nsig = 36
            } else if ((expr == 'ANT') | (expr == 'NAT')){
                nsig = 18
            } else if ((expr == 'GHG') | (expr == 'No-GHG')){
                nsig = 18
            } else if ((expr == 'AER') | (expr == 'No-AER')){
                nsig = 15
            } else if (expr == 'No-AER-GHG'){
                nsig = 15
            }
	} else if (ensemble_id == 'one') {
            if (expr == 'historical'){
                nsig = 17
            } else if ((expr == 'ANT') | (expr == 'NAT')){
                nsig = 6
            } else if ((expr == 'GHG') | (expr == 'No-GHG')){
                nsig = 6
            } else if ((expr == 'AER') | (expr == 'No-AER')){
                nsig = 5
            } else if (expr == 'No-AER-GHG'){
                nsig = 5
            }
        } else if (ensemble_id == 'limited_all'){
            if (expr == 'historical'){
                nsig = 36
            } else if ((expr == 'ANT') | (expr == 'NAT')){
                nsig = 12
            } else if ((expr == 'GHG') | (expr == 'No-GHG')){
                nsig = 12
            } else if ((expr == 'AER') | (expr == 'No-AER')){
                nsig = 12
            } else if (expr == 'No-AER-GHG'){
                nsig = 12
            }
        } else if (ensemble_id == 'limited_large'){
            if (expr == 'historical'){
                nsig = 30
            } else if ((expr == 'ANT') | (expr == 'NAT')){
                nsig = 12
            } else if ((expr == 'GHG') | (expr == 'No-GHG')){
                nsig = 12
            } else if ((expr == 'AER') | (expr == 'No-AER')){
                nsig = 12
            } else if (expr == 'No-AER-GHG'){
                nsig = 12
            }
        } else if (ensemble_id == 'uniform_all'){
            if (expr == 'historical'){
                nsig = 15
            } else if ((expr == 'ANT') | (expr == 'NAT')){
                nsig = 15
            } else if ((expr == 'GHG') | (expr == 'No-GHG')){
                nsig = 15
            } else if ((expr == 'AER') | (expr == 'No-AER')){
                nsig = 15
            } else if (expr == 'No-AER-GHG'){
                nsig = 15
            }
        } else if (ensemble_id == 'uniform_large'){
            if (expr == 'historical'){
                nsig = 15
            } else if ((expr == 'ANT') | (expr == 'NAT')){
                nsig = 15
            } else if ((expr == 'GHG') | (expr == 'No-GHG')){
                nsig = 15
            } else if ((expr == 'AER') | (expr == 'No-AER')){
                nsig = 15
            } else if (expr == 'No-AER-GHG'){
                nsig = 15
            }
        }
    }
    if ((ensemble_id != 'uniform_all') & (ensemble_id != 'uniform_large')){
        if (remove_bcc){
            if (expr == 'piControl'){
                nsig = nsig - 1
            } else{
                nsig = nsig - 3 # note there are three ensemble members
            }
        }
        if (remove_cesm_cesmw){
            if (expr == 'historical'){
                nsig = nsig - 6
            } else if (expr == 'piControl'){
                nsig = nsig - 2
            }
        }
        if (remove_noresm){
            if ((expr != 'AER') & (expr != 'No-AER') &
                (expr != 'No-AER-GHG') & (expr != 'piControl')){
                nsig = nsig - 3
            }
        }
    }
    return (nsig)
}


for (rg in tslist){
    ##path.root = paste('/lustre/haven/user/ywang254/AI/output/da2/',
    ##                  rg, sep = '')
    path.root = paste('/lustre/haven/proj/UTK0134/AI/output/da2/',
                      ensemble_path, '/', rg, sep = '')

for (signals in signals.list){
    signal.list = paste(signals, collapse = '_')
    tmp = file.path(path.root, 'graph', signal.list)
    if (! file.exists(tmp)) dir.create(tmp)

for (var in vlist){
    tmp = file.path(path.root, 'graph', signal.list, var)
    if (! file.exists(tmp)) dir.create(tmp)

for (aggr in rtslist){
    tmp = file.path(path.root, 'graph', signal.list, var, aggr)
    if (! file.exists(tmp)) dir.create(tmp)

for (tsmooth in tlist){
    tmp = file.path(path.root, 'graph', signal.list, var, aggr, tsmooth)
    if (! file.exists(tmp)) dir.create(tmp)

    ## yclim specified here!
    #if (tsmooth == 6){
    #    yclim = seq(1973, 2003)
    #} else {
    #    yclim = seq(1971, 2004)
    #}

for (yrbrac in blist){
    yrbrac.name = paste(yrbrac[1], yrbrac[length(yrbrac)], sep = '-')

    if (yrbrac[1] == 1948){
        yrbrac = seq(1949, 2014)
    }

    print(paste(rg, signal.list, var, aggr, tsmooth, yrbrac.name))

    #
    file.obssig = file.path(path.root, 'obs_sig', paste(var, aggr, tsmooth,
                            paste(yrbrac.name, 'dat', sep = '.'),
                            sep = '_'))
    file.noise1 = file.path(path.root, 'noise1', paste(var, aggr, tsmooth,
                            paste(yrbrac.name, 'dat', sep = '.'),
                            sep = '_'))
    file.noise2 = file.path(path.root, 'noise2', paste(var, aggr, tsmooth,
                            paste(yrbrac.name, 'dat', sep = '.'),
                            sep = '_'))
    path.out = file.path(path.root, 'graph', signal.list, var, aggr, tsmooth)

    # ---- subset of relevant signals
    whichcol = c()
    for (s in signals){ whichcol = c(whichcol, which(elist == s)) }

    # Loop over the observations.
    for (o in 1:length(olist)){
        print(olist[o])

        Z<-readin(file.obssig, file.noise1, file.noise2,
                  whichcol + length(olist), o)

        if (double_noise){
            Z@noise1 = 2 * Z@noise1
            Z@noise2 = 2 * Z@noise2
        }

        #
        png(file.path(path.out,
                      paste(paste('plot1', yrbrac.name, olist[o], suffix,
                                  sep = '_'),
                            'png', sep = '.')),
            width = 960, height = 960)
        par(new=TRUE)
        plot(seq(1, length(Z@Y)), Z@Y, type='l', xlab='Year', ylab = var,
             col = 1, ylim = c(min(Z@Y), max(Z@Y)))
        for (x in 1:dim(Z@X)[2]){
            par(new=TRUE)
            plot(seq(1, length(Z@Y)), Z@X[,x], type='l', xlab = 'Year',
                 ylab = var, col = 1 + x, ylim = c(min(Z@Y), max(Z@Y)))
        }
        legend(5, -0.008, legend=c('OBS', signals), col=seq(1, dim(Z@X)[2]+1),
               lty=rep(1,  length(signals)))
        dev.off()

        # Which spaces are climatology.
        yrlist = seq(yrbrac[1], yrbrac[length(yrbrac)], tsmooth)
        nt = length(yrlist) # num. time dimensions
        ns = length(Z@Y) / length(yrlist) # num. spatial dimentions
        p1 = min(which(yrlist >= yclim[1]))
        p2 = max(which(yrlist <= yclim[length(yclim)]))
        u=redop(nt, p1, p2)
        Zr=redECOF(Z, u, nt, ns, timefirst=ncol(u))

        # OLS-DA in reduced data space.
        nsig = as.vector(sapply(signals, get.nsig, yrbrac))

        # Checkpoint 1: Need variability to be large enough from zero.
        if (! validZ(Zr)){
            png(file.path(path.out,
                          paste(paste('plot4_ols', yrbrac.name, olist[o],
                                      suffix, sep = '_'), 'png', sep = ".")),
                width = 1080, height = 720)
            par(new = TRUE)
            plot(1, 1)
            text(1, 1, 'STD(Y/X/noise1/noise2) = 0, cannot perform D&A')
            dev.off()

            png(file.path(path.out,
                          paste(paste('plot4_tls', yrbrac.name, olist[o],
                                      suffix, sep = "_"), 'png', sep = '.')),
                width = 1080, height = 720)
            par(new = TRUE)
            plot(1, 1)
            text(1, 1, 'STD(Y/X/noise1/noise2) = 0, cannot perform D&A')
            dev.off()
            next
        }

        time.start.ols <- Sys.time()
        # Checkpoint 2
        o1.ols = try(ols(Zr@Y, Zr@X, Zr@noise1, Zr@noise2, nsig = nsig),
                     silent = TRUE)
        if (class(o1.ols) == "try-error"){
            msg = geterrmessage()
            if (grepl('Too few large eigenvalues', msg) |
                grepl('Eigenvalue < 0', msg)){
                print('Caught OLS error')
                png(file.path(path.out,
                              paste(paste('plot4_ols', yrbrac.name, olist[o],
                                          suffix, sep = '_'), 'png',
                                    sep = ".")),
                    width = 1080, height = 720)
                par(new = TRUE)
                plot(1, 1)
                text(1, 1, 'Noise matrix does not have sufficient rank, cannot perform D&A')
                dev.off()
                next
            } else {
                stop(msg)
            }
        }
        time.end.ols <- Sys.time()
        print(time.end.ols - time.start.ols)
        write.csv(o1.ols,
                  file = file.path(path.out,
                                   paste(paste("ols", yrbrac.name, olist[o],
                                               suffix, sep = '_'), 'csv',
                                         sep = ".")),
                  na = "", quote = FALSE, row.names = FALSE)

        png(file.path(path.out,
                      paste(paste('plot4_ols', yrbrac.name, olist[o],
                                  suffix, sep = '_'), 'png', sep = ".")),
            width = 1080, height = 720)
        par(mfrow=c(2, length(signals)))
        # Checkpoint 3
        for (n in c(1:length(signals))){
            beta.low = paste(paste('beta', n, sep = ''), 'low', sep = '_')
            beta.hat = paste(paste('beta', n, sep = ''), 'hat', sep = '_')
            beta.up = paste(paste('beta', n, sep = ''), 'up', sep = '_')

            o1.ols[ abs(o1.ols[,beta.low]) == Inf, beta.low] = NA
            o1.ols[ abs(o1.ols[,beta.hat]) == Inf, beta.hat] = NA
            o1.ols[ abs(o1.ols[,beta.up]) == Inf, beta.up] = NA
            if (all(is.na(o1.ols[,beta.hat]))){
                plot(1, 1)
                text(1, 1, 'Estimated Scaling Factor is NaN.')
                dev.off()
                next
            } else {
                if (all(is.na(o1.ols[,beta.low]))){
                    o1.ols[,beta.low] = o1.ols[,beta.hat] - 999.
                }
                if (all(is.na(o1.ols[,beta.up]))){
                    o1.ols[,beta.up] = o1.ols[,beta.hat] + 999.
                }
            }
        }
        plotbetas(o1.ols)
        plotrstat(o1.ols)
        dev.off()

        # TLS-DA in reduced data space. With regularization of noise.
        time.start.tls <- Sys.time()
        # Checkpoint 4 (REG = TRUE/FALSE)
        o1.tls = try(tls.A03(Zr@Y, Zr@X, Zr@noise1, Zr@noise2, nsig = nsig,
                             REG = FALSE), silent = TRUE)
        if (class(o1.tls) == "try-error"){
            msg = geterrmessage()
            if (grepl('Too few large eigenvalues', msg)){
                print('Caught TLS error')
                png(file.path(path.out,
                              paste(paste('plot4_tls', yrbrac.name, olist[o],
                                          suffix, sep = '_'), 'png',
                                    sep = '.')),
                    width = 1080, height = 720)
                par(new = TRUE)
                plot(1, 1)
                text(1, 1, 'Noise matrix does not have sufficient rank, cannot perform D&A')
                dev.off()
                next
            } else {
                stop(msg)
            }
        }
        time.end.tls  <- Sys.time()
        print(time.end.tls - time.start.tls)
        write.csv(o1.tls,
                  file = file.path(path.out,
                                   paste(paste("tls", yrbrac.name, olist[o],
                                               suffix, sep = '_'), 'csv',
                                         sep = ".")),
                  na = "", quote = FALSE, row.names = FALSE)

        png(file.path(path.out,
                      paste(paste('plot4_tls', yrbrac.name, olist[o],
                                  suffix, sep = "_"), 'png', sep = '.')),
            width = 1080, height = 720)
        par(mfrow=c(2, length(signals)))
        # Checkpoint 5
        for (n in c(1:length(signals))){
            beta.low = paste(paste('beta', n, sep = ''), 'low', sep = '_')
            beta.hat = paste(paste('beta', n, sep = ''), 'hat', sep = '_')
            beta.up = paste(paste('beta', n, sep = ''), 'up', sep = '_')

            o1.tls[ (abs(o1.tls[,beta.low]) == Inf), beta.low] = NA
            o1.tls[ (abs(o1.tls[,beta.up]) == Inf), beta.up] = NA
            o1.tls[ (abs(o1.tls[,beta.hat]) == Inf), beta.hat] = NA
            if (all(is.na(o1.tls[, beta.hat]))){
                plot(1, 1)
                text(1, 1, 'Estimated Scaling Factor is NaN.')
                dev.off()
                next
            } else {
                if (all(is.na(o1.tls[, beta.low]))){
                    o1.tls[, beta.low] = o1.tls[, beta.hat] - 999.
                }
                if (all(is.na(o1.tls[,beta.up]))){
                    o1.tls[, beta.up] = o1.tls[, beta.hat] + 999.
                }
            }
        }
        plotbetas(o1.tls)
        plotrstat(o1.tls)
        dev.off()
    
        # Regularized optimal fingerprint method, that assumes that the
        # signals are not perfectly known.
        ##o1.rof<-tls.ROF(Zr@Y, Zr@X, Zr@noise1, Zr@noise2, nsig = nsig)
        ##o1.rof
    }
}
}
}
}
}
}
