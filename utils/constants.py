import numpy as np

elist = ['historical', 'ANT', 'GHG', 'AER', 'NAT', 'No-AER', 'No-GHG',
         'No-AER-GHG']
olist = ['CRU', 'GPCC', 'UDEL', 'PREC', 'GLDAS', '20CR', 'ERA5', 'Avg']
olist_ts = ['CRU-PRE+GLDAS-PET', 'CRU-PRE+20CR-PET', 'CRU-PRE+ERA5-PET',
            'GPCC-PRE+GLDAS-PET', 'GPCC-PRE+20CR-PET', 'GPCC-PRE+ERA5-PET',
            'UDEL-PRE+GLDAS-PET', 'UDEL-PRE+20CR-PET', 'UDEL-PRE+ERA5-PET',
            'PREC-REC-PRE+GLDAS-PET', 'PREC-REC-PRE+20CR-PET',
            'PREC-REC-PRE+ERA5-PET']
#            'Skip', 'Skip', 'Skip',

## --- the following olist_ts will result in skipping UDEL
##olist_ts = ['Obs', 'Obs', 'Skip', 'Obs', 'EC', 'EC', 'Skip', 'EC']


#vlist = ['Arid_234', 'Arid_1', 'Arid_5', 'AI']
vlist = ['AI']

#alist_global = ['lat', 'global', 'hemisphere', 'NH', 'SH']
g3list = ['global', 'NH', 'SH']
alist_global = ['lat', 'ts']
alist_7region = ['lat', 'ts']
alist_da3 = ['nc', 'lat']
r7list = ['Eastern Asia', 'Europe', 'North American', 'Northern Africa',
          'Oceania', 'South American', 'Southern Africa']
r7lat1 = [20, -25, 37, 9, -37, 20, -45]
r7lat2 = [58,  5, 60,  27, -5, 55, -10]
r7lon1 = [-127, -83, 10, -18, -10, 90, 112]
r7lon2 = [ -51, -33, 52,  36,  70, 147, 155]
tslist = ['Global', 'NH', 'SH']
## , 'Eastern Asia', 'Europe', 'North American',
## 'Northern Africa', 'Oceania', 'South American', 'Southern Africa']
##tslist = ['Global', '31-70N', '0-30N', 'SH']
rtslist = ['TS']

########
# DA2
########
# methods
mthd = ['ols', 'tls']
# Years to smooth over
##tlist = [6, 12] # 3 is too short
tlist = [5, 10]
##tlist = [6, 11]
# Year brackets
# Not much signal seems to be present for range(1907, 2015), range(1943, 2015)
##blist = [range(1967, 2015)] # range(1979, 2015) - use the longer period
#blist = [range(1965, 2015), range(1967, 2017)]
blist = [range(1965, 2015)] # temporary test 20200309
#blist = [range(1949, 2015)]
# Year to remove the climatology from
##yclim = range(1967, 2003) # Must be compatible with tlist
yclim = {5: range(1975, 2006), 10: range(1975, 2006)} # temporary test 20200309
##yclim = {6: range(1973,2003), 11: range(1971,2004)}

##da2_signals = ['AI-ALL', 'AI-ANT', 'AI-GHG', 'AI-AER', 'AI-NAT',
##               'AI-ANT_AI-NAT', 'AI-GHG_AI-AER']
##da2_signals_single = ['AI-ALL', 'AI-ANT', 'AI-GHG', 'AI-AER', 'AI-NAT']
##da2_signals_duo = ['AI-ANT_AI-NAT', 'AI-GHG_AI-AER']
##da2_signals = ['AI-ALL', 'AI-ANT_AI-NAT', 'AI-GHG_AI-AER_AI-OTH_AI-NAT']
da2_signals = ['historical', 'ANT', 'GHG', 'AER', 'NAT', 
               'No-AER', 'No-GHG', 'No-AER-GHG', 'ANT_NAT',
               'GHG_No-GHG_NAT', 'AER_No-AER_NAT', 'GHG_AER_No-AER-GHG_NAT']
da2_signals_1 = ['historical', 'ANT', 'GHG', 'AER', 'NAT', 
                 'No-AER', 'No-GHG', 'No-AER-GHG']
da2_signals_2 = ['ANT_NAT']
da2_signals_3 = ['GHG_No-GHG_NAT', 'AER_No-AER_NAT']
da2_signals_4 = ['GHG_AER_No-AER-GHG_NAT']


########
# The ensemble members in TimeSeries files.
########
# CanESM5, EC-Earth3-Veg
ts_model_id = {'r1i1p1f1': {'historical': ['BCC-CSM2-MR',
                                           'IPSL-CM6A-LR', 'MIROC6',
                                           'MRI-ESM2-0', 'FGOALS-g3',
                                           'NorESM2-LM', 'ACCESS-CM2',
                                           'ACCESS-ESM1-5', 'CESM2',
                                           'CESM2-WACCM', 'EC-Earth3',
                                           'FGOALS-f3-L',
                                           'GFDL-ESM4', 'INM-CM4-8',
                                           'INM-CM5-0', 'MPI-ESM1-2-HR',
                                           'MPI-ESM1-2-LR'],
                            'GHG': ['BCC-CSM2-MR', 'IPSL-CM6A-LR', 'MIROC6',
                                    'MRI-ESM2-0', 'FGOALS-g3', 'NorESM2-LM'],
                            'NAT': ['BCC-CSM2-MR',
                                    'IPSL-CM6A-LR', 'MIROC6', 'MRI-ESM2-0',
                                    'FGOALS-g3', 'NorESM2-LM'],
                            'AER': ['BCC-CSM2-MR', 
                                    'IPSL-CM6A-LR', 'MIROC6', 'MRI-ESM2-0', 
                                    'FGOALS-g3'],
                            'piControl': ['ACCESS-CM2',
                                          'BCC-CSM2-MR', 
                                          'INM-CM4-8', 'IPSL-CM6A-LR',
                                          'MIROC6', 'MRI-ESM2-0', 
                                          'ACCESS-ESM1-5', 'CESM2',
                                          'CESM2-WACCM', 'EC-Earth3',
                                          'INM-CM5-0',
                                          'MPI-ESM1-2-HR', 'MPI-ESM1-2-LR',
                                          'FGOALS-f3-L']},
               'r2i1p1f1': {'historical': ['BCC-CSM2-MR', 'IPSL-CM6A-LR',
                                           'MIROC6', 'MRI-ESM2-0',
                                           'FGOALS-g3', 'NorESM2-LM',
                                           'ACCESS-CM2', 'CESM2',
                                           'CESM2-WACCM', 'EC-Earth3',
                                           'FGOALS-f3-L', 'INM-CM5-0',
                                           'MPI-ESM1-2-HR', 'MPI-ESM1-2-LR'],
                            'GHG': ['BCC-CSM2-MR', 'IPSL-CM6A-LR', 'MIROC6',
                                    'MRI-ESM2-0', 'FGOALS-g3', 'NorESM2-LM'],
                            'NAT': ['BCC-CSM2-MR',
                                    'IPSL-CM6A-LR', 'MIROC6', 'MRI-ESM2-0',
                                    'FGOALS-g3', 'NorESM2-LM'],
                            'AER': ['BCC-CSM2-MR', 
                                    'IPSL-CM6A-LR', 'MIROC6', 'MRI-ESM2-0', 
                                    'FGOALS-g3'],
                            'piControl': []},
               'r3i1p1f1': {'historical': ['BCC-CSM2-MR', 'IPSL-CM6A-LR',
                                           'MIROC6', 'MRI-ESM2-0', 'FGOALS-g3',
                                           'NorESM2-LM', 'CESM2',
                                           'CESM2-WACCM', 
                                           'FGOALS-f3-L', 'INM-CM5-0', 
                                           'MPI-ESM1-2-HR', 'MPI-ESM1-2-LR'],
                            'ssp585': ['BCC-CSM2-MR', 'FGOALS-g3',
                                       'IPSL-CM6A-LR', 'MRI-ESM2-0', 'MIROC6',
                                       'NorESM2-LM', 'CESM2', 'CESM2-WACCM',
                                       'FGOALS-f3-L',
                                       'INM-CM5-0', 'MPI-ESM1-2-HR'
                                       'MPI-ESM1-2-LR'],
                            'GHG': ['BCC-CSM2-MR', 'IPSL-CM6A-LR', 'MIROC6',
                                    'MRI-ESM2-0', 'FGOALS-g3', 'NorESM2-LM'],
                            'NAT': ['BCC-CSM2-MR',
                                    'IPSL-CM6A-LR', 'MIROC6', 'MRI-ESM2-0',
                                    'FGOALS-g3', 'NorESM2-LM'],
                            'AER': ['BCC-CSM2-MR', 
                                    'IPSL-CM6A-LR', 'MIROC6', 'MRI-ESM2-0', 
                                    'FGOALS-g3'],
                            'piControl': []}}
for idd in ['r1i1p1f1', 'r2i1p1f1', 'r3i1p1f1']:
    ts_model_id[idd]['ANT'] = ts_model_id[idd]['NAT']
    ts_model_id[idd]['No-GHG'] = ts_model_id[idd]['GHG']
    ts_model_id[idd]['No-AER'] = ts_model_id[idd]['AER']
    ts_model_id[idd]['No-AER-GHG'] = list(set(ts_model_id[idd]['GHG']) & \
                                          set(ts_model_id[idd]['AER']))
