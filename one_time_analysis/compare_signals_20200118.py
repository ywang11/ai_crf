"""
Interpretation: The trend in AI does separate when the time draws nearer to
end of century. AER signal is very positive during Mid-century, but becomes
more like the NAT signal later. GHG signal is negative since Mid-century. 
ANT and AI are like NAT in Mid-century, but becomes like GHG in late 20th
century.
"""


import os
import xarray as xr
import matplotlib.pyplot as plt
from glob import glob
import pandas as pd
from matplotlib.cm import get_cmap
import numpy as np
from scipy.stats import linregress


def plot_ts_shade(ax, time, matrix, ts_label = '', ts_col = 'red',
                  shade_col = 'red', alpha = 0.2, skipna = False):
    """
    Plot a shaded ensemble.
    """
    if skipna:
        ts_min = np.nanmin(matrix, axis = 1)
        ts_mean = np.nanmean(matrix, axis = 1)
        ts_max = np.nanmax(matrix, axis = 1)
    else:
        ts_min = np.min(matrix, axis = 1)
        ts_mean = np.mean(matrix, axis = 1)
        ts_max = np.max(matrix, axis = 1)

    hl, = ax.plot(time, ts_mean, '-', color = ts_col, linewidth = 2,
                  label = ts_label)
    ax.plot(time, ts_min, '--', color = shade_col, linewidth = 1)
    ax.plot(time, ts_max, '--', color = shade_col, linewidth = 1)
    ax.fill_between(time, ts_min, ts_max, where = ts_max > ts_min,
                    facecolor = shade_col, alpha = alpha)
    hfill, = ax.fill(np.nan, np.nan, facecolor = shade_col, alpha = alpha)
    return hl, hfill


path_data = '/lustre/haven/user/ywang254/AI/data'
elist = ['AI', 'AI-AER', 'AI-ANT', 'AI-GHG', 'AI-NAT'] # experiments


df_list = {}
for e in elist:
    flist = glob(os.path.join(path_data, e, '*.nc'))

    df_list[e] = pd.DataFrame()
    for f in flist:
        temp = f.split('_')[-1].split('-')
        yr = range(int(temp[0][:4]), 
                   int(temp[1][:4]) + 1) # time dimension of data

        model = f.split('_')[-3] + '_' + f.split('_')[-2] # model + id

        hf = xr.open_dataset(f)
        df_list[e][model] = pd.Series(hf['ai'].mean(dim = ['lat',
                                                           'lon']).values,
                                      index = yr)
        hf.close()



# Plot AI
cmap = get_cmap('Spectral')
clist = [cmap((i+0.5)/ len(elist)) for i in range(len(elist))]

fig, ax = plt.subplots(figsize = (15, 6))

h = [None] * len(elist)

for e_ind, e in enumerate(elist):
    h[e_ind] = plot_ts_shade(ax, df_list[e].index, df_list[e].values, 
                             ts_col = clist[e_ind], 
                             shade_col = clist[e_ind])

ax.legend(h, elist, loc = (0.5, -0.2), ncol = 3)
ax.set_xlim([1950, 2020])

fig.savefig('/lustre/haven/user/ywang254/AI/one_time_analysis/' + \
            'compare_signals_20200118-1.png', dpi = 600.,
            bbox_inches = 'tight')
plt.close(fig)


# 50-year trend, moving window
n = 50
trend_list = {}
for e in elist:
    trend_list[e] = df_list[e].iloc[(n-1):, :].copy(deep = True)
    for model in df_list[e].columns:
        for yr in df_list[e].index[(n-1):]:
            temp = df_list[e].loc[(yr-n+1):yr, model]
            result = linregress(temp.index, temp.values)
            trend_list[e].loc[yr, model] = result.slope


# Plot 50-year trend, moving window
cmap = get_cmap('rainbow')
clist = [cmap((i+0.5)/ len(elist)) for i in range(len(elist))]

fig, ax = plt.subplots(figsize = (15, 6))

h = [None] * len(elist)

for e_ind, e in enumerate(elist):
    h[e_ind] = plot_ts_shade(ax, trend_list[e].index, trend_list[e].values, 
                             ts_col = clist[e_ind], 
                             shade_col = clist[e_ind])

ax.legend(h, elist, loc = (0.5, -0.2), ncol = 3)
ax.set_xlim([1950, 2020])
ax.set_xlabel('Last year of 50-year window')
ax.set_ylabel('Trend (m3/m3/year)')

fig.savefig('/lustre/haven/user/ywang254/AI/one_time_analysis/' + \
            'compare_signals_20200118-2.png', dpi = 600.,
            bbox_inches = 'tight')
plt.close(fig)
