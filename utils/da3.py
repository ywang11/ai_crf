import pandas as pd
import numpy as np
from warnings import warn


def judge(fnoise, fsignal, p = 0.9):
    """
    Given noise and signal file names, determine the significance of the 
    signal. Assume the rows of signal files are different signals 
    (ALL, ANT, GHG, AER, etc.), and the columns are different observations.

    1 - the signal correlation is significantly higher than the noise
        correlation.
    0 - the signal correlation is statistically indistinguishable from the 
        noise correlation.
    -1 - the signal correlation is significantly lower than the noise
        correlation.
    -2 - the signal is NaN.
    """
    noise = pd.read_csv(fnoise, index_col = 0)

    # 
    if np.sum(np.isnan(noise.values)):
        warn('Noise contains NaN values')
        noise.dropna(axis = 0, how = 'all', inplace = True)

    signal = pd.read_csv(fsignal, index_col = 0)

    side = (1-p)/2 * 100

    situ = pd.DataFrame(data = np.nan, index = signal.index,
                        columns = signal.columns)
    for i in signal.columns:
        CI_low = np.percentile(noise.loc[:, i].values, side)
        CI_up = np.percentile(noise.loc[:, i].values, 100 - side)

        situ.loc[signal.loc[:, i].values > CI_up, i] = 1
        situ.loc[signal.loc[:, i].values <= CI_up, i] = 0
        situ.loc[signal.loc[:, i].values <= CI_low, i] = 0

    situ.replace(np.nan, -2, inplace = True)

    return situ
