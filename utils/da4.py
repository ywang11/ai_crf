"""
20200227

Detection and attribution using Pearson correlation coefficients between 
 (obs, signal) (obs, piControl) space-time series.

Gudmundsson, L., Seneviratne, S. & Zhang, X. Anthropogenic climate change 
  detected in European renewable freshwater resources. Nature Clim Change 7,
  813–816 (2017). https://doi.org/10.1038/nclimate3416
"""
import xarray as xr
from scipy.stats import pearsonr
import pandas as pd
import numpy as np
import types
import multiprocessing as mp


class da4:
    # filenames
    nnoise = [] 
    nsignal = {}
    nobs = {}

    # data for obs; Assume to be small compared to the rest.
    obs = {}

    # correlations.
    corr_noise_obs = pd.DataFrame()
    corr_signal_obs = {}

    # p-confidence interval of corr(noise, obs)
    ci = pd.DataFrame()

    # situation: -1 means corr(signal, obs) is smaller than the lower
    #               confidence interval of corr(noise, obs), 
    #            0 means corr(signal, obs) is within the confidence interval
    #              of corr(noise, obs),
    #            1 means corr(signal, obs) is above the upper confidence
    #              interval of corr(noise, obs), which is desirable.
    situ = pd.DataFrame()

    # misc parameters
    ndim = 0
    period = None
    mask = None
    vname = ''
    readf_kw = {}
    p = 0.9

    def __init__(self, noise = [], signal = {}, obs = {}, ndim = [2,3],
                 p = 0.9, period = None, mask = None, vname = '', 
                 readf_kw = {}):
        """
        noise: list of filenames for noise
        signal: dictionary of {experiment: list of filenames}
        obs: dictionary of observational data
        ndim: number of dimensions in the data
        period: subperiod of data to conduct the D&A on
        mask: subset in space of the data to conduct the D&A on
        readf_kw: additional argument to pass into reading the noise & signal
                  & obs files.

        if ndim == 2, assume csv files, first column is time; 
        if ndim == 3, assume NetCDF files, leftmost dimension 'time', 
                      and vname must be provided.
        """
        self.nnoise = noise
        self.nsignal = signal
        self.nobs = obs

        self.corr_noise_obs = pd.DataFrame(data = np.nan, 
                                           index = [],
                                           columns = list(obs.keys()))

        self.corr_signal_obs = pd.DataFrame(data = np.nan, 
                                            index = list(signal.keys()), 
                                            columns = list(obs.keys()))

        self.ci = pd.DataFrame(data = np.nan, index = ['CI_low', 'CI_up'],
                               columns = list(obs.keys()))

        self.situ = pd.DataFrame(data = np.nan, index = list(signal.keys()),
                                 columns = list(obs.keys()))

        self.ndim = ndim
        self.period = period
        self.mask = mask
        self.vname = vname
        self.readf_kw = readf_kw
        self.p = p

    def __read_f(self, f, ndim):
        if ndim == 3:
            hr = xr.open_mfdataset(f, **self.readf_kw)
            var = hr[self.vname].copy(deep = True).load()
            hr.close()
        else:
            var = pd.read_csv(f, **self.readf_kw)
        return var

    def read_obs(self):
        for on in self.nobs.keys():
            self.obs[on] = self.__read_f(self.nobs[on], self.ndim)
            if self.ndim == 3:
                self.obs[on] = self.obs[on].loc[self.period, :, :]
            else:
                self.obs[on] = self.obs[on].loc[self.period, :]

    def fit_noise(self):
        count = 0
        for nn in self.nnoise:
            var = self.__read_f(nn, self.ndim)
            ##print(var)

            var = var.values.reshape(var.shape[0], -1)

            #print(var.shape[0])
            #print(len(self.period))

            # Pre-estimate NaN in var otherwise too slow.
            temp1 = np.isfinite(var)
            temp2 = np.sum(temp1, axis = 1)

            for t in range(var.shape[0] - len(self.period) + 1):
                print(count, nn, t)

                x = var[t:(t + len(self.period)), :].reshape(-1)

                # INM-CM4-8 model somehow has missing values after year 144.
                # Minimum number of required points is 3 for regression.
                if sum(temp2[t:(t+len(self.period))]) < 4:
                    continue

                for on in self.nobs.keys():
                    y = self.obs[on].values.reshape(-1)
                    print(y)

                    temp = temp1[t:(t+len(self.period))].reshape(-1) & \
                           np.isfinite(y)
                    self.corr_noise_obs.loc[count, on], _ = pearsonr(x[temp],
                                                                     y[temp])
                count += 1
        return self.corr_noise_obs

    def fit_signal(self):
        for ns in self.nsignal.keys():
            if isinstance(self.nsignal[ns], list):
                # {signal: {model: }}
                for ind, nsns in enumerate(self.nsignal[ns]):
                    print(ns, nsns)
                    if ind == 0:
                        var = self.__read_f(nsns, self.ndim)
                    else:
                        var += self.__read_f(nsns, self.ndim)
                var /= (ind + 1)
            elif isinstance(self.nsignal[ns], dict):
                # {signal: {model: {ensemble member: }}}
                for ind, nsns in enumerate(self.nsignal[ns]):
                    for ind2, nsnsns in enumerate(self.nsignal[ns][nsns]):
                        print(ns, nsns, nsnsns)
                        if ind2 == 0:
                            var2 = self.__read_f(nsnsns, self.ndim)
                        else:
                            var2 += self.__read_f(nsnsns, self.ndim)
                    var2 /= (ind2 + 1)
                    if ind == 0:
                        var = var2
                    else:
                        var += var2
                var /= (ind + 1)
            elif isinstance(self.nsignal[ns], str):
                print(ns)
                var = self.__read_f(self.nsignal[ns], self.ndim)
            else:
                raise Exception('Unrecognized file type')

            if self.ndim == 3:
                x = var.loc[self.period, :, :].values.reshape(-1)
            else:
                x = var.loc[self.period, :].values.reshape(-1)
            for on in self.nobs.keys():
                y = self.obs[on].values.reshape(-1)
                temp = np.isfinite(x) & np.isfinite(y)
                self.corr_signal_obs.loc[ns, on], _ = pearsonr(x[temp],
                                                               y[temp])
        return self.corr_signal_obs

    def judge(self):
        side = np.round((1-self.p)/2 * 100)

        for on in self.nobs.keys():
            temp = self.corr_noise_obs.loc[:, on].values
            self.ci.loc[:, on] = np.nanpercentile(temp, [side, 100 - side])

            self.situ.loc[:, on] = np.where(self.corr_signal_obs[on].values > \
                                            self.ci.loc['CI_up', on], 1, 
                                            self.situ[on].values)
            self.situ.loc[:, on] = np.where(self.corr_signal_obs[on].values \
                                            <= self.ci.loc['CI_up', on], 0,
                                            self.situ[on].values)
            self.situ.loc[:, on] = np.where(self.corr_signal_obs[on].values < \
                                            self.ci.loc['CI_low', on], -1, 
                                            self.situ[on].values)
        return self.situ, self.ci


def run_1(params, fcn, fcs, fj):
    try:
        obj = da4(**params)
        obj.read_obs()
        cno = obj.fit_noise()
        cso = obj.fit_signal()
        st, c = obj.judge()

        if fcn:
            cno.to_csv(fcn)
        if fcs:
            cso.to_csv(fcs)
        if fj:
            st.to_csv(fj[0])
            c.to_csv(fj[1])

        return True
    except:
        return False


class da4_ensemble:
    run_status = {}
    fc_list = []
    init_params = []
    save_corr_noise = []
    save_corr_signal = []
    save_judge = []

    def __init__(self):
        pass

    def ensemble_run(self, fc_list = [], init_params = [],
                     save_corr_noise = None, save_corr_signal = None, 
                     save_judge = None, njobs = 1):
        """
        Conduct the procedure for all factors.

        fc_list: list of tuples giving the factors. 
        init_params: (1) list of dictionaries for D&A parameters corresponding
                         to each tuple in fc_list.
                     (2) dictionary of dictionaries to access D&A parameters
                         corresponding to each tuple in fc_list.
                     (3) function to build dictionary of D&A parameters
                         corresponding to each tuple in fc_list.
        save_<corr_noise/corr_signal/judge>: 
                     (1) list of filenames to save the corr_noise/corr_signal/
                         judge files corresponding to each tuple in fc_list.
                     (2) dict of filenames to save the corr_noise/corr_signal/
                         judge files corresponding to each tuple in fc_list.
                     (3) function to get the filenames from each tuple in
                         fc_list to save the corr_noise/corr_signal/judge 
                         files.
                     If None, do not save the intermediate files.
        njobs: if 1, loop through fc_list in serial. Otherwise, map to 
               multiple processors.
        """
        self.fc_list = fc_list

        #
        if (init_params != None):
            if isinstance(init_params, types.FunctionType):
                self.init_params = [init_params(fc) for fc in fc_list]
            elif isinstance(init_params, dict):
                self.init_params = [init_params[fc] for fc in fc_list]
            else:
                self.init_params = init_params
        else:
            self.init_params = [[] for fc in fc_list]

        if (save_corr_noise != None):
           if isinstance(save_corr_noise, types.FunctionType):
               self.save_corr_noise = [save_corr_noise(fc) for fc in fc_list]
           elif isinstance(save_corr_noise, dict):
               self.save_corr_noise = [save_corr_noise[fc] for fc in fc_list]
           else:
               self.save_corr_noise = save_corr_noise
        else:
            self.save_corr_noise = [[] for fc in fc_list]

        if (save_corr_signal != None):
           if isinstance(save_corr_signal, types.FunctionType):
               self.save_corr_signal = [save_corr_signal(fc) for fc in fc_list]
           elif isinstance(save_corr_signal, dict):
               self.save_corr_signal = [save_corr_signal(fc) for fc in fc_list]
           else:
               self.save_corr_signal = save_corr_signal
        else:
            self.save_corr_signal = [[] for fc in fc_list]

        if (save_judge != None):
            if isinstance(save_judge, types.FunctionType):
                self.save_judge = [save_judge(fc) for fc in fc_list]
            elif isinstance(save_judge, types.FunctionType):
                self.save_judge = [save_judge(fc) for fc in fc_list]
            else:
                self.save_judge = save_judge
        else:
            self.save_judge = [[] for fc in fc_list]

        #
        if njobs == 1:
            for ind, fc in enumerate(fc_list):
                print(ind)
                self.run_status[ind] = run_1(self.init_params[ind],
                                             self.save_corr_noise[ind],
                                             self.save_corr_signal[ind],
                                             self.save_judge[ind])
        else:
            p = mp.Pool(njobs)
            self.run_status = dict([(ind, 
                p.apply_async(run_1, args = (self.init_params[ind],
                                             self.save_corr_noise[ind],
                                             self.save_corr_signal[ind],
                                             self.save_judge[ind]))) \
                                    for ind in range(len(fc_list))])
            p.close()
            p.join()

        return self.run_status


    def ensemble_summary(self, fout = None):
        # Create a table based on the run results.
        for ind, fc in enumerate(self.fc_list):
            fj = pd.read_csv(self.save_judge[ind][0], index_col = 0).unstack()
            if ind == 0:
                summary = pd.DataFrame(data = np.nan,
                        index = pd.MultiIndex.from_tuples(self.fc_list),
                                       columns = fj.index)
            summary.loc[fc, fj.index] = fj

        if not fout == None:
            summary.to_csv(fout)

        return summary
