"""
20200225

Remove the GHG and AER part from the ANT experiments.

OTH = ANT - GHG - AER
"""
import os
import pandas as pd
import itertools as it
import numpy as np
from glob import glob
from utils.constants import *
from utils.settings import *
from utils.da import *


name = 'OTH'


###############################################################################
# Global
###############################################################################
for rg, aggr, var in it.product(['global'], alist_global, vlist):
    flist_ant = get_fdict('AI-ANT', aggr, var, rg)
    flist_ghg = get_fdict('AI-GHG', aggr, var, rg)
    flist_aer = get_fdict('AI-AER', aggr, var, rg)

    # Overlapping models.
    models = list(set(flist_ant.keys()) & set(flist_ghg.keys()) & \
                  set(flist_aer.keys()))

    for m in models:
        ant = read_f(flist_ant[m][0], aggr, rg, 
                     skip_lat5 = True, skip_hemi = True)
        ghg = read_f(flist_ghg[m][0], aggr, rg, 
                     skip_lat5 = True, skip_hemi = True)
        aer = read_f(flist_aer[m][0], aggr, rg,
                     skip_lat5 = True, skip_hemi = True)

        oth = ant - ghg - aer

        ##print(flist_ghg[m][0].replace('GHG','OTH'))

        if aggr == 'ts':
            oth.to_csv(flist_ghg[m][0].replace('GHG','OTH'), index = False,
                       header = None)
        elif aggr == 'lat':
            oth.to_csv(flist_ghg[m][0].replace('GHG','OTH'), index = False)


###############################################################################
# 7-region
###############################################################################
for rg, aggr, var in it.product(r7list, alist_7region, vlist):
    flist_ant = get_fdict('AI-ANT', aggr, var, rg)
    flist_ghg = get_fdict('AI-GHG', aggr, var, rg)
    flist_aer = get_fdict('AI-AER', aggr, var, rg)

    # Overlapping models.
    if aggr == 'lat':
        models = list(set(flist_ant.keys()) & set(flist_ghg.keys()) & \
                      set(flist_aer.keys()))
        #print(models)

        for m in models:
            ant = read_f(flist_ant[m][0], aggr, rg,
                         skip_lat5 = True, skip_hemi = True)
            ghg = read_f(flist_ghg[m][0], aggr, rg,
                         skip_lat5 = True, skip_hemi = True)
            aer = read_f(flist_aer[m][0], aggr, rg,
                         skip_lat5 = True, skip_hemi = True)
            oth = ant - ghg - aer

            ##print(flist_ghg[m][0].replace('GHG','OTH'))
    
            oth.to_csv(flist_ghg[m][0].replace('GHG','OTH'), index = False)
    else:
        models = ['BCC-CSM2-MR', 'CanESM5', 'IPSL-CM6A-LR', 'MIROC6', 
                  'MRI-ESM2-0']
        ant = read_f(flist_ant[0], aggr, rg, 
                     skip_lat5 = True, skip_hemi = True)
        ghg = read_f(flist_ghg[0], aggr, rg, 
                     skip_lat5 = True, skip_hemi = True)
        aer = read_f(flist_aer[0], aggr, rg, 
                     skip_lat5 = True, skip_hemi = True)

        oth = ant.iloc[:, [0,1,3,5,4]] - ghg.iloc[:, [0,1,4,5,6]].values - \
              aer.iloc[:, [0,1,3,4,5]].values

        ##print(flist_ghg[0].replace('GHG','OTH'))

        oth.to_csv(flist_ghg[0].replace('GHG','OTH'), index = False,
                   header = None)
