"""
20200929

Create a table to summarize whether the scaling factors is above 1
 and covers 1.
Dependency: "da2_ts_figure3_data.py", ols & tls results, different ensemble
  members.
"""
import pandas as pd
from utils.constants import *
from utils.settings import *
import os
import itertools as it
from utils.da2 import judge
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from utils.plotting import mergecells
import matplotlib as mpl
from matplotlib.cm import get_cmap


aggr = 'TS'
var = 'AI'
tsmooth = '5' # '5', '6'
yrbrac_n = '1965-2014' # '1965-2014', '1948-2014'
suffix = '_single_noise'


#
signal_list = ['historical', 'ANT', 'GHG', 'AER', 'NAT', 'NoAER',
               'NoGHG']
signal_list_name = {'historical': 'ALL',
                    'ANT': 'ANT', 'GHG': 'GHG',
                    'AER': 'AER', 'NAT': 'NAT',
                    'NoAER': 'ANTNoAER',
                    'NoGHG': 'ANTNoGHG'}
abbrev_list = ['DA', 'D', 'N']
region_list = ['Global', 'NH', 'SH']
n_pass = pd.DataFrame(data = 0,
                      index = [signal_list_name[ss] for ss in signal_list],
                      columns = pd.MultiIndex.from_product([region_list,
                                                            abbrev_list]))


# Rating
# 1 - the CI is above 0, and covers 1
# 2 - the CI is above 1
# 3 - otherwise
def judge(sf):
    sf_judge = np.full(sf.shape[1], 0, dtype = int)
    for j in range(sf.shape[1]):
        if sf.loc['low', :].values[j] > 1:
            sf_judge[j] = 2
        elif (sf.loc['up', :].values[j] >= 1) & \
             (sf.loc['low', :].values[j] <= 1) & \
             (sf.loc['low', :].values[j] > 0):
            sf_judge[j] = 1
        else:
            sf_judge[j] = 3
    return sf_judge


for ss, rg in it.product(signal_list, region_list):
    for obs, method in it.product(, ['ols', 'tls']):
        beta_all = pd.concat([ \
            pd.read_csv(os.path.join(path_out, 'da2',
                                     'run_1_all_ensemble_members',
                                     method + '_ts_fig3_sf_' + var + '_' + \
                                     aggr + '_' + tsmooth + '_' + \
                                     yrbrac_n + suffix + '.csv'),
                        index_col = [0, 1, 2], header = [0, 1] \
            ).stack().stack(),
            pd.read_csv(os.path.join(path_out, 'da2',
                                     'run_4_all_ensemble_members',
                                     method + '_ts_fig3_sf_' + var + '_' + \
                                     aggr + '_' + tsmooth + '_' + \
                                     yrbrac_n + suffix + '.csv'),
                        index_col = [0, 1, 2], header = [0, 1] \
            ).stack().stack(),
            pd.read_csv(os.path.join(path_out, 'da2',
                                     'run_3_large_ensemble_members',
                                     method + '_ts_fig3_sf_' + var + '_' + \
                                     aggr + '_' + tsmooth + '_' + \
                                     yrbrac_n + suffix + '.csv'),
                        index_col = [0, 1, 2], header = [0, 1] \
            ).stack().stack()], axis = 1)
        beta_all.columns = ['All models', 'Limited models', '3-member models']

        #
        beta = beta_all.loc[(slice(None), ss, slice(None), obs, rg), :]
        beta.index = beta.index.droplevel([1,3,4])
        beta = beta.reorder_levels([1,0]).sort_index().unstack()

        #
        sf_judge = judge(beta)

        #
        ar, counts = np.unique(sf_judge, return_counts = True)

        #
        for aind, aa in enumerate(ar):
            n_pass.loc[signal_list_name[ss],
                       (rg, abbrev_list[aa-1])] += counts[aind]
n_pass.to_csv(os.path.join(path_out_da(path_out, 'da2'),
                           'n_pass' + suffix + '.csv'))
