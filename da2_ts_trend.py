"""
20200226

Summarize the time series results from da2.
Calculate the observed and attributable trends.
"""
import pandas as pd
from utils.constants import *
from utils.settings import *
from utils.da2 import attributed_trend
import os
import itertools as it
import numpy as np


blist_n = []
for x in blist:
    if x[0] == 1949:
        blist_n.append('1948-2014')
    else:
        blist_n.append(str(x[0]) + '-' + str(x[-1]))

en_list = ['all', 'one', 'large', 'uniform_all', 'uniform_large',
           'limited_all', 'limited_large']
for ensemble, double_noise, rg, var, aggr, tsmooth, yrbrac_n in \
    it.product(en_list, [True, False], tslist, vlist, rtslist, tlist, blist_n):

    if double_noise:
        suffix = '_double_noise'
    else:
        suffix = '_single_noise'

    f_obs_sig = os.path.join(path_out_da('da2', rg, ensemble), 'obs_sig',
                             var + '_' + aggr + '_' + str(tsmooth) + \
                             '_' + yrbrac_n + '.dat')
    for da2_signals, nsig in zip([da2_signals_1,da2_signals_2,
                                  da2_signals_3, da2_signals_4],
                                 ['one', 'two', 'three', 'four']):
        count = 0
        for method, obs in it.product(mthd, olist):
            flist_beta = [os.path.join(path_out_da('da2', rg, ensemble),
                                       'graph', expr, var, aggr, str(tsmooth), 
                                       method + '_' + yrbrac_n + '_' + obs + \
                                       suffix + '.csv') \
                          for expr in da2_signals]
            trend, trend_obs = attributed_trend(f_obs_sig, flist_beta, 
                                                da2_signals, tsmooth, 
                                                yrbrac_n)
            if count == 0:
                # (1) trend_obs_sig is duplicated and only needs to save once
                trend_obs.to_csv(os.path.join(path_out_da('da2', rg,
                                                          ensemble),
                                              'trend', nsig + \
                                              '_' + var + '_' + aggr + \
                                              '_' + str(tsmooth) + '_' + \
                                              yrbrac_n + '_obs' + suffix + \
                                              '.csv'))
                # (2)
                trend_sig = pd.DataFrame(data = np.nan,
                                         index = pd.MultiIndex.from_product( \
                            [mthd, olist, list(trend.index)]),
                                         columns = trend.columns)

            trend_sig.loc[(method, obs), :] = trend.values
            count += 1
        trend_sig.to_csv(os.path.join(path_out_da('da2', rg, ensemble),
                                      'trend', nsig + '_' + var + '_' + \
                                      aggr + '_' + str(tsmooth) + '_' + \
                                      yrbrac_n + '_sig' + suffix + '.csv'))
