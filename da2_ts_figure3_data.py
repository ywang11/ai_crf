"""
20200226

Save the scaling factors and attributed changes from global, NH, SH of da2
 to table.
"""
import pandas as pd
from utils.constants import *
from utils.settings import *
import os
import itertools as it
from utils.da2 import judge


alist = rtslist

blist_n = [str(x[0]) + '-' + str(x[-1]) for x in blist]
aggr = 'TS'
var = 'AI'
tsmooth = '5' # '5'/'10' '6'
yrbrac_n = '1965-2014' # '1965-2014' '1948-2014'


beta_summary = pd.DataFrame(data = np.nan,
                            columns = pd.MultiIndex.from_product([tslist,
                                                                  olist]),
                            index = pd.MultiIndex.from_tuples( \
                                [x + (y,) for x in [('1-factor', 'historical'),
                                                    ('1-factor', 'ANT'),
                                                    ('1-factor', 'GHG'),
                                                    ('1-factor', 'AER'),
                                                    ('1-factor', 'NAT'),
                                                    ('2-factor', 'ANT'),
                                                    ('2-factor', 'NAT'),
                                                    ('3-factor', 'AER'),
                                                    ('3-factor', 'NoAER'),
                                                    ('3-factor', 'NATaer'),
                                                    ('3-factor', 'GHG'),
                                                    ('3-factor', 'NoGHG'),
                                                    ('3-factor', 'NATghg')] \
                                 for y in ['low', 'hat', 'up']]))
trend_summary = pd.DataFrame(data = np.nan,
                             columns = pd.MultiIndex.from_product([tslist,
                                                                   olist]),
                             index = pd.MultiIndex.from_tuples( \
                                [x + (y,) for x in [('Obs', 'Obs'),
                                                    ('1-factor', 'historical'),
                                                    ('1-factor', 'ANT'),
                                                    ('1-factor', 'GHG'),
                                                    ('1-factor', 'AER'),
                                                    ('1-factor', 'NAT'),
                                                    ('2-factor', 'ANT'),
                                                    ('2-factor', 'NAT'),
                                                    ('3-factor', 'AER'),
                                                    ('3-factor', 'NoAER'),
                                                    ('3-factor', 'NATaer'),
                                                    ('3-factor', 'GHG'),
                                                    ('3-factor', 'NoGHG'),
                                                    ('3-factor', 'NATghg')] \
                                 for y in ['low', 'hat', 'up']]))


en_list = ['all', 'large', 'one', 'uniform_all', 'uniform_large',
           'limited_all', 'limited_large']
for ensemble, method, suffix in it.product(en_list,
                                           ['ols', 'tls'],
                                           ['_double_noise', '_single_noise']):
    for count, rg in enumerate(tslist):
        #######################################################################
        # 1. Scaling factors
        #######################################################################
        # (1) One signal, historical, ANT, GHG, AER, NAT
        beta_1 = pd.read_csv(os.path.join(path_out_da('da2', rg, ensemble),
                                          'trend', 'one_' + var + '_' + \
                                          aggr + '_' + str(tsmooth) + \
                                          '_' + yrbrac_n + '_sf' + \
                                          suffix + '.csv'),
                             index_col = [0,1], header = None)
        # Somehow specifying header and columns and the same time
        # does not work.
        header = list(beta_1.iloc[:3, :].values)
        beta_1 = beta_1.iloc[3:, :]
        beta_1.columns = pd.MultiIndex.from_arrays(header)

        # (2) Two signal
        beta_2 = pd.read_csv(os.path.join(path_out_da('da2', rg, ensemble),
                                          'trend', 'two_' + var + '_' + \
                                          aggr + '_' + str(tsmooth) + \
                                          '_' + yrbrac_n + '_sf' + \
                                          suffix + '.csv'),
                             index_col = [0,1], header = None)
        # Somehow specifying header and columns and the same time
        # does not work.
        header = list(beta_2.iloc[:3, :].values)
        beta_2 = beta_2.iloc[3:, :]
        beta_2.columns = pd.MultiIndex.from_arrays(header)

        # (3) Three signal
        beta_3 = pd.read_csv(os.path.join(path_out_da('da2', rg, ensemble),
                                          'trend', 'three_' + var + '_' + \
                                          aggr + '_' + str(tsmooth) + \
                                          '_' + yrbrac_n + '_sf' + \
                                          suffix + '.csv'),
                             index_col = [0,1], header = None)
        # Somehow specifying header and columns and the same time
        # does not work.
        header = list(beta_3.iloc[:3, :].values)
        beta_3 = beta_3.iloc[3:, :]
        beta_3.columns = pd.MultiIndex.from_arrays(header)
    
        beta_1 = beta_1.astype(float)
        beta_2 = beta_2.astype(float)
        beta_3 = beta_3.astype(float)
    
        # (4) Save to table
        for oind, obs in enumerate(olist):
            # One signal
            for sind, sig in enumerate(['historical', 'ANT', 'GHG', 'AER',
                                        'NAT']):
                beta_summary.loc[('1-factor', sig), (rg, obs)] = \
                    beta_1.loc[(method, obs), (sig, 'beta_1')].values
    
            # Two signal
            beta_summary.loc[('2-factor', 'ANT'), (rg, obs)] = \
                beta_2.loc[(method, obs), ('ANT_NAT', 'beta_1')].values
            beta_summary.loc[('2-factor', 'NAT'), (rg, obs)] = \
                beta_2.loc[(method, obs), ('ANT_NAT', 'beta_2')].values
    
            # Three signal
            beta_summary.loc[('3-factor', 'AER'), (rg, obs)] = \
                beta_3.loc[(method, obs), ('AER_No-AER_NAT', 'beta_1')].values
            beta_summary.loc[('3-factor', 'NoAER'), (rg, obs)] = \
                beta_3.loc[(method, obs), ('AER_No-AER_NAT', 'beta_2')].values
            beta_summary.loc[('3-factor', 'NATaer'), (rg, obs)] = \
                beta_3.loc[(method, obs), ('AER_No-AER_NAT', 'beta_3')].values
    
            beta_summary.loc[('3-factor', 'GHG'), (rg, obs)] = \
                beta_3.loc[(method, obs), ('GHG_No-GHG_NAT', 'beta_1')].values
            beta_summary.loc[('3-factor', 'NoGHG'), (rg, obs)] = \
                beta_3.loc[(method, obs), ('GHG_No-GHG_NAT', 'beta_2')].values
            beta_summary.loc[('3-factor', 'NATghg'), (rg, obs)] = \
                beta_3.loc[(method, obs), ('GHG_No-GHG_NAT', 'beta_3')].values
    
        #######################################################################
        # 2. Attributable trend
        #######################################################################
        # (1) One signal, historical, ANT, GHG, AER, NAT
        obs_trend = pd.read_csv(os.path.join(path_out_da('da2', rg, ensemble),
                                             'trend', 'one_' + var + '_' + \
                                             aggr + '_' + str(tsmooth) + \
                                             '_' + yrbrac_n + '_obs' + \
                                             suffix + '.csv'),
                                index_col = 0, header = 0)
        trend_1 = pd.read_csv(os.path.join(path_out_da('da2', rg, ensemble),
                                           'trend', 'one_' + var + '_' + \
                                           aggr + '_' + str(tsmooth) + \
                                           '_' + yrbrac_n + '_sig' + \
                                           suffix + '.csv'),
                              index_col = [0,1,2], header = None)
        # Somehow specifying header and columns and the same time
        # does not work.
        header = list(trend_1.iloc[:3, :].values)
        trend_1 = trend_1.iloc[3:, :]
        trend_1.columns = pd.MultiIndex.from_arrays(header)

        # (2) Two signal
        trend_2 = pd.read_csv(os.path.join(path_out_da('da2', rg, ensemble),
                                           'trend', 'two_' + var + '_' + \
                                           aggr + '_' + str(tsmooth) + \
                                           '_' + yrbrac_n + '_sig' + \
                                           suffix + '.csv'),
                              index_col = [0,1,2], header = None)
        # Somehow specifying header and columns and the same time
        # does not work.
        header = list(trend_2.iloc[:3, :].values)
        trend_2 = trend_2.iloc[3:, :]
        trend_2.columns = pd.MultiIndex.from_arrays(header)
    
        # (3) Three signal
        obs_trend_3 = pd.read_csv(os.path.join(path_out_da('da2', rg,
                                                           ensemble),
                                               'trend', 'three_' + var + \
                                               '_' + aggr + '_' + \
                                               str(tsmooth) + \
                                               '_' + yrbrac_n + '_obs' + \
                                               suffix + '.csv'),
                                  index_col = 0, header = 0)
        trend_3 = pd.read_csv(os.path.join(path_out_da('da2', rg, ensemble),
                                           'trend', 'three_' + var + '_' + \
                                           aggr + '_' + str(tsmooth) + \
                                           '_' + yrbrac_n + '_sig' + \
                                           suffix + '.csv'),
                              index_col = [0,1,2], header = None)
        # Somehow specifying header and columns and the same time
        # does not work.
        header = list(trend_3.iloc[:3, :].values)
        trend_3 = trend_3.iloc[3:, :]
        trend_3.columns = pd.MultiIndex.from_arrays(header)
    
        trend_1 = trend_1.astype(float)
        trend_2 = trend_2.astype(float)
        trend_3 = trend_3.astype(float)
    
        # (4) Save to trend
        for oind, obs in enumerate(olist):
            # Obs
            trend_summary.loc[('Obs', 'Obs', 'hat'), (rg, obs)] = \
                obs_trend.loc[0, obs]
    
            # One signal
            for sind, sig in enumerate(['historical', 'ANT', 'GHG', 'AER',
                                        'NAT']):
                trend_summary.loc[('1-factor', sig), (rg, obs)] = \
                    trend_1.loc[(method, obs, 0), (sig, 'beta_1')].values

            # Two signal
            trend_summary.loc[('2-factor', 'ANT'), (rg, obs)] = \
                trend_2.loc[(method, obs, 0), ('ANT_NAT', 'beta_1')].values
            trend_summary.loc[('2-factor', 'NAT'), (rg, obs)] = \
                trend_2.loc[(method, obs, 0), ('ANT_NAT', 'beta_2')].values
    
            # Three signal
            trend_summary.loc[('3-factor', 'AER'), (rg, obs)] = \
                trend_3.loc[(method, obs, 0), ('AER_No-AER_NAT',
                                               'beta_1')].values
            trend_summary.loc[('3-factor', 'NoAER'), (rg, obs)] = \
                trend_3.loc[(method, obs, 0), ('AER_No-AER_NAT',
                                               'beta_2')].values
            trend_summary.loc[('3-factor', 'NATaer'), (rg, obs)] = \
                trend_3.loc[(method, obs, 0), ('AER_No-AER_NAT',
                                               'beta_3')].values
    
            trend_summary.loc[('3-factor', 'GHG'), (rg, obs)] = \
                trend_3.loc[(method, obs, 0), ('GHG_No-GHG_NAT',
                                               'beta_1')].values
            trend_summary.loc[('3-factor', 'NoGHG'), (rg, obs)] = \
                trend_3.loc[(method, obs, 0), ('GHG_No-GHG_NAT',
                                               'beta_2')].values
            trend_summary.loc[('3-factor', 'NATghg'), (rg, obs)] = \
                trend_3.loc[(method, obs, 0), ('GHG_No-GHG_NAT',
                                               'beta_3')].values
    
    beta_summary.to_csv(os.path.join(path_out_da('da2', '', ensemble),
                                     method + '_ts_fig3_sf_' + \
                                     var + '_' + aggr + '_' + str(tsmooth) + \
                                     '_' + yrbrac_n + suffix + '.csv'))
    trend_summary.to_csv(os.path.join(path_out_da('da2', '', ensemble),
                                      method + '_ts_fig3_trend_' + \
                                      var + '_' + aggr + '_' + str(tsmooth) + \
                                      '_' + yrbrac_n + suffix + '.csv'))
