"""
20190110

Create tab-delimited inputs to R calculation.

<type of signal, ALL/GHG/AER/NAT/multiple>_obs_sig.dat
noise1_piC.dat
noise2_piC.dat
"""
from utils.settings import *
from utils.constants import *
from utils.da import *
import os
import pandas as pd
import itertools as it
import numpy as np
import multiprocessing as mp
import warnings
warnings.filterwarnings("error")


# MODIFY
# 'BCC-CSM2-MR', 'CESM2', 'CESM2-WACCM', 'NorESM2-LM',
# 'CanESM5', 'EC-Earth3-Veg', 'MRI-ESM2-0', 'INM-CM4-8'
remove_list = [] # ['CanESM5', 'EC-Earth3-Veg']


e = 'piControl'
enlist = ['all', 'one', 'large', 'uniform_all', 'uniform_large',
          'limited_all', 'limited_large']
for ensemble, var, rg, aggr, tsmooth, yrbrac in it.product(enlist,
                                                           vlist, tslist,
                                                           rtslist,
                                                           tlist, blist):
    nyears = len(yrbrac) // tsmooth
    yccc = yclim[tsmooth]

    if yrbrac[0] == 1949:
        yrstr = '1948-2014'
        df = read_ts(yrstr, var, rg, e, aggr, ensemble, remove_list).values
    else:
        yrstr = str(yrbrac[0]) + '-' + str(yrbrac[-1])
        df = read_ts(yrstr, var, rg, e, aggr, ensemble, remove_list).values

    noise_list = []
    # create noise segments
    for i in range(df.shape[0] - len(yrbrac) + 1):
        tmp = df[i:(i + len(yrbrac)), :]
        # ---- remove climatology
        tmp = tmp - np.mean(tmp[(np.array(yrbrac) >= yccc[0]) & \
                                (np.array(yrbrac) <= yccc[-1]), :], axis = 0)
        tmp = np.mean(tmp.reshape(-1, tsmooth, tmp.shape[-1]), axis = 1)
        #print(tmp)
        for m in range(tmp.shape[-1]):
            noise_list.append(tmp[:,m])

    ##print(noise_list)

    f = open(os.path.join(path_out_da('da2', rg,  ensemble), 'noise1',
                          var + '_' + aggr + '_' + str(tsmooth) + '_' + \
                          yrstr + '.dat'), 'w+')
    for rr in range(len(noise_list) // 2):
        temp = ','.join(['%.5f' % x for x in noise_list[rr]])
        f.write(temp + '\n')
    f.close()

    f = open(os.path.join(path_out_da('da2', rg, ensemble), 'noise2',
                          var + '_' + aggr + '_' + str(tsmooth) + '_' + \
                          yrstr + '.dat'), 'w')
    for rr in range(len(noise_list) // 2, len(noise_list) // 2 * 2):
        temp = ','.join(['%.5f' % x for x in noise_list[rr]])
        f.write(temp + '\n')
    f.close()
