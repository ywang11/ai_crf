"""
20190110

Create tab-delimited inputs to R calculation.

<type of signal, ALL/GHG/AER/NAT/multiple>_obs_sig.dat
noise1_piC.dat
noise2_piC.dat
"""
from utils.settings import *
from utils.constants import *
from utils.da import *
import os
import pandas as pd
import itertools as it
import numpy as np
import multiprocessing as mp
import warnings
warnings.filterwarnings("error")


# MODIFY
remove_list = []                              # 'BCC-CSM2-MR', 'CESM2',
                                              # 'CESM2-WACCM', 'NorESM2-LM'
                                              # 'CanESM5', 'EC-Earth3-Veg'
                                              # 'INM-CM4-8'

en_list =['all', 'one', 'large', 'uniform_all', 'uniform_large',
          'limited_all', 'limited_large']
for ensemble, var, rg, aggr, tsmooth, yrbrac in it.product(en_list,
                                                           vlist, tslist,
                                                           rtslist, tlist,
                                                           blist):
    if yrbrac[0] == 1949:
        yrstr = '1948-2014'
    else:
        yrstr = str(yrbrac[0]) + '-' + str(yrbrac[-1])
    yccc = yclim[tsmooth]

    # observation
    obs_list = []
    df = read_ts(yrstr, var, rg, 'Observation', aggr, files = ensemble)
    if yrbrac[0] == 1949:
        df = df.loc[1:, :]

    df.index = yrbrac
    # remove the climatology
    df = (df - df.loc[yccc, :].mean(axis = 0)).values
    for o in olist[:-1]:
        which = np.array([o in y for y in olist_ts])
        #print(which)
        tmp = df[:, which].mean(axis = 1)
        ##print(tmp)
        obs_list.append(np.mean(tmp.reshape(-1, tsmooth), axis = 1))
    which = np.array([np.sum([o in y for o in olist]) > 0 \
                      for y in olist_ts])
    #print(which)
    obs_list.append(np.mean(df[:, which].mean(axis = 1 \
                                              ).reshape(-1,tsmooth),
                            axis = 1))

    # signal
    sig_list = []
    for e in elist:
        ##print(e)
        df = read_ts(yrstr, var, rg, e, aggr, ensemble, remove_list)
        if yrbrac[0] == 1949:
            df = df.loc[1:, :]
        df.index = yrbrac

        print(ensemble, e, df.shape[1])
        
        # remove the climatology
        df = (df - df.loc[yccc, :].mean(axis = 0)).values
        #
        df = np.mean(df.reshape(-1, tsmooth, df.shape[-1]),
                     axis = 1).mean(axis = 1)
        ##print(df)
        #
        sig_list.append(df)

    # write to file
    f = open(os.path.join(path_out_da('da2', rg, ensemble), 'obs_sig',
                          var + '_' + aggr + '_' + str(tsmooth) + \
                          '_' + yrstr + '.dat'), 'w')
    for rr in range(len(obs_list)):
        temp = ','.join(['%.5f' % x for x in obs_list[rr]])
        f.write(temp + '\n')
    for rr in range(len(sig_list)):
        temp = ','.join(['%.5f' % x for x in sig_list[rr]])
        f.write(temp + '\n')
    f.close()
