"""
20200226

Summarize the time series results from da2.
Calculate the observed and attributable trends.
"""
import pandas as pd
from utils.constants import *
from utils.settings import *
from utils.da2 import judge_beta
import os
import itertools as it
import numpy as np


blist_n = []
for x in blist:
    if x[0] == 1949:
        blist_n.append('1948-2014')
    else:
        blist_n.append(str(x[0]) + '-' + str(x[-1]))

en_list = ['all', 'one', 'large', 'uniform_all', 'uniform_large',
           'limited_all', 'limited_large']
for ensemble, double_noise, rg, var, aggr, tsmooth, yrbrac_n in \
    it.product(en_list, [True, False], tslist, vlist, rtslist, tlist, blist_n):

    if double_noise:
        suffix = '_double_noise'
    else:
        suffix = '_single_noise'

    f_obs_sig = os.path.join(path_out_da('da2', rg, ensemble), 'obs_sig',
                             var + '_' + aggr + '_' + str(tsmooth) + \
                             '_' + yrbrac_n + suffix + '.dat')

    for da2_signals, nsig, nn in zip([da2_signals_1, da2_signals_2,
                                      da2_signals_3, da2_signals_4],
                                     ['one', 'two', 'three', 'four'],
                                     [1, 2, 3, 4]):
        #print(nn)
        beta_summary = pd.DataFrame(data = np.nan,
            index = pd.MultiIndex.from_product([mthd, olist]),
            columns = pd.MultiIndex.from_product([da2_signals,
                                                  ['beta_' + str(n+1) \
                                                   for n in range(nn)],
                                                  ['low', 'hat', 'up']]))
        for method, obs, expr in it.product(mthd, olist, da2_signals):
            f_beta = os.path.join(path_out_da('da2', rg, ensemble), 'graph',
                                  expr, var, aggr, str(tsmooth),
                                  method + '_' + yrbrac_n + '_' + obs + \
                                  suffix + '.csv')
            beta = judge_beta(f_beta, expr, tsmooth, yrbrac_n)

            #print(beta)
            beta_summary.loc[(method, obs), (expr, slice(None))] = beta
        beta_summary.to_csv(os.path.join(path_out_da('da2', rg, ensemble),
                                         'trend', nsig + '_' + var + '_' + \
                                         aggr + '_' + str(tsmooth) + \
                                         '_' + yrbrac_n + '_sf' + \
                                         suffix + '.csv'))
