"""
20200413

Plot the scaling factors and attributed changes from global, NH, SH, and
  7-region results to da2.

Dependency: "da2_ts_figure3_data.py", ols & tls results, different ensemble
  members.

20210413

Changed to read new data (Avg)
"""
import pandas as pd
from utils.constants import *
from utils.settings import *
import os
import itertools as it
from utils.da2 import judge
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from utils.plotting import mergecells
import matplotlib as mpl
from matplotlib.cm import get_cmap


alist = rtslist
blist_n = [str(x[0]) + '-' + str(x[-1]) for x in blist]
#clist = ['#d7191c', '#2c7bb6', '#1a9641']
cmap = get_cmap('tab10')
clist0 = [cmap(i) for i in np.arange(0., 1., 1/10)]
clist0 = [clist0[i] for i in [0,1,3,4,2,5,6]]
clist = dict(zip(['historical', 'ANT', 'GHG', 'AER', 'NAT', 'NoAER',
                  'NoGHG'], clist0))
clist['ALL'] = clist['historical']
clist['ANTnoAER'] = clist['NoAER']
clist['ANTnoGHG'] = clist['NoGHG']
clist['NATaer'] = clist['NAT']
clist['NATghg'] = clist['NAT']
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'

aggr = 'TS'
var = 'AI'
tsmooth = '5' # '5', '6'
yrbrac_n = '1965-2014' # '1965-2014', '1948-2014'

mpl.rcParams['font.size'] = 7.
mpl.rcParams['xtick.labelsize'] = 7.
mpl.rcParams['axes.titlesize'] = 7.

for method, suffix in it.product(['ols', 'tls'], 
                                 ['_double_noise', '_single_noise']):

    beta_all = pd.read_csv(os.path.join(path_out, 'da2',
        'run_6_all_ensemble_members', method + '_ts_fig3_sf_' + var + '_' + \
        aggr + '_' + tsmooth + '_' + yrbrac_n + suffix + '.csv'),
        index_col = [0, 1, 2], header = [0, 1]).stack(dropna = False \
        ).stack(dropna = False)

    trend_all = pd.read_csv(os.path.join(path_out, 'da2',
        'run_6_all_ensemble_members',
        method + '_ts_fig3_trend_' + var + '_' + \
        aggr + '_' + tsmooth + '_' + yrbrac_n + suffix + '.csv'),
        index_col = [0, 1, 2], header = [0, 1]).stack(dropna = False \
        ).stack(dropna = False)


    fig, axes = plt.subplots(figsize = (6.5, 2.*len(tslist)),
                             nrows = len(tslist), ncols = 2,
                             sharex = False, sharey = False)
    fig.subplots_adjust(wspace = 0.25, hspace = 0.1)
    for count, rg in enumerate(tslist):
        #######################################################################
        # 1. Scaling factors
        #######################################################################
        ax = axes[count, 0]

        h = []
        # One signal
        for sind, sig in enumerate(['historical', 'ANT', 'GHG', 'AER', 'NAT']):
            x = 1.5 + 2 * sind
            beta = beta_all.loc[('1-factor', sig, slice(None),
                                 slice(None), rg)]
            beta.index = beta.index.droplevel([0,1,4])
            beta = beta.drop(['EC'], level = 1).unstack()
            beta = beta.T

    #        if (rg == 'SH') & (sind == 'AER'):
    #            dummy()
    
            # skip the weird values
            beta.loc[(beta['hat'].values < beta['low'].values) | \
                     (beta['hat'].values > beta['up'].values), :] = np.nan
            #beta = beta.dropna(axis = 0, how = 'all')
    
            if beta.empty:
                continue

            clist_sub = [clist[sig],
                         np.append(clist[sig][:3], 0.5)]

            ax.scatter(x + np.array([-0.4,0.4]), beta['hat'].values,
                       color = clist_sub, marker = 'o', s = 8, lw = 0)

            #if (method == 'tls') & (rg == 'SH') & (sig == 'NAT'):
            #    # Manually skip bad estimates.
            #    continue
            #ax.errorbar(x + np.array([-0.4,0.4]), beta['hat'].values,
            #            yerr = np.array([beta['hat'].values - \
            #            beta['low'].values,
            #                             beta['up'].values - \
            #            beta['hat'].values]),
            #            ecolor = clist_sub, elinewidth = 1., lw = 0)
            for m in range(2):
                if np.isnan(beta['hat'].values[m]):
                    continue
                ax.errorbar(x + (m-.5) * 0.8, beta['hat'].values[m],
                            yerr = np.array([[beta['hat'].values[m] - \
                                              beta['low'].values[m]],
                                             [beta['up'].values[m] - \
                                              beta['hat'].values[m]]]),
                            ecolor = clist_sub[m], elinewidth = 1, lw = 0)
    
        # Two signal
        for bind, bn in enumerate(['ANT', 'NAT']):
            x = 12.5 + 2 * bind
            beta = beta_all.loc[('2-factor', bn, slice(None),
                                 slice(None), rg)]
            beta.index = beta.index.droplevel([0,1,4])
            beta = beta.drop(['EC'], level = 1).unstack()            
            beta = beta.T
            if beta.empty:
                continue
    
            # skip the weird values
            beta.loc[(beta['hat'].values < beta['low'].values) | \
                     (beta['hat'].values > beta['up'].values), :] = np.nan
            #beta = beta.dropna(axis = 0, how = 'all')
    
            clist_sub = [clist[bn], np.append(clist[bn][:3], 0.5)]

            ax.scatter(x + np.array([-0.4,0.4]), beta['hat'].values,
                       color = clist_sub, marker = 'o', s = 8, lw = 0)
            ax.errorbar(x + np.array([-0.4,0.4]), beta['hat'].values,
                        yerr = np.array([beta['hat'].values - \
                                         beta['low'].values,
                                         beta['up'].values - \
                                         beta['hat'].values]),
                        ecolor = clist_sub, elinewidth = 1., lw = 0)
    
        # Three signal
        for bind, bn in enumerate(['AER', 'NoAER', 'NATaer']):
            x = 17.5 + 2 * bind
            beta = beta_all.loc[('3-factor', bn, slice(None),
                                 slice(None), rg)]
            beta.index = beta.index.droplevel([0,1,4])
            beta = beta.drop(['EC'], level = 1).unstack()            
            beta = beta.T
            if beta.empty:
                continue
    
            # skip the weird values
            beta.loc[(beta['hat'].values < beta['low'].values) | \
                     (beta['hat'].values > beta['up'].values), :] = np.nan
            #beta = beta.dropna(axis = 0, how = 'all')
    
            clist_sub = [clist[bn], np.append(clist[bn][:3], 0.5)]

            ax.scatter(x + np.array([-0.4,0.4]), beta['hat'].values,
                       color = clist_sub, marker = 'o', s = 8, lw = 0)
            ax.errorbar(x + np.array([-0.4,0.4]), beta['hat'].values,
                        yerr = np.array([beta['hat'].values - \
                                         beta['low'].values,
                                         beta['up'].values - \
                                         beta['hat'].values]),
                        ecolor = [clist_sub[ind] for ind in \
                                  np.where(~np.isnan(beta['hat'].values))[0]],
                        elinewidth = 1., lw = 0)
    
        for bind, bn in enumerate(['GHG', 'NoGHG', 'NATghg']):
            x = 24.5 + 2 * bind
            beta = beta_all.loc[('3-factor', bn, slice(None),
                                 slice(None),rg)]
            beta.index = beta.index.droplevel([0,1,4])
            beta = beta.drop(['EC'], level = 1).unstack()
            beta = beta.T
            if beta.empty:
                continue

            # skip the weird values
            beta.loc[(beta['hat'].values < beta['low'].values) | \
                     (beta['hat'].values > beta['up'].values), :] = np.nan
            #beta = beta.dropna(axis = 0, how = 'all')
    
            clist_sub = [clist[bn], np.append(clist[bn][:3], 0.5)]

            ax.scatter(x + np.array([-0.4,0.4]), beta['hat'].values,
                       color = clist_sub, marker = 'o', s = 8, lw = 0)
            ax.errorbar(x + np.array([-0.4,0.4]), beta['hat'].values,
                        yerr = [beta['hat'].values - beta['low'].values,
                                beta['up'].values - beta['hat'].values],
                        ecolor = [clist_sub[ind] for ind in \
                                  np.where(~np.isnan(beta['hat'].values))[0]],
                        elinewidth = 1., lw = 0)

        ax.set_ylim([-5, 8])
        #ax.set_yticks(np.arange(-100, 101, 10))
        #ax.set_ylim([-3, 3])
        ax.set_yticks(np.arange(-4, 7.1, 2.))
        ax.axhline(0, ls = '--', color = 'grey', lw = 0.5)
        ax.axhline(1, ls = '--', color = 'grey', lw = 0.5)
        ax.axvline(11, ls = '--', color = 'k', lw = 0.5)
        ax.axvline(16, ls = '--', color = 'k', lw = 0.5)
        ax.axvline(23, ls = '--', color = 'k', lw = 0.5)
        ax.text(-0.1, 0.95, lab[count*2],
                transform = ax.transAxes, weight = 'bold')
    
        ax.set_xlim([0., 24.5 + 2*3])
        ax.set_xticks(list(np.arange(1.5, 1.5 + 2 * 5, 2)) + \
                      list(np.arange(12.5, 12.5 + 2 * 2, 2)) + \
                      list(np.arange(17.5, 17.5 + 2 * 3, 2)) + \
                      list(np.arange(24.5, 24.5 + 2 * 3, 2)))
    
        if count == (len(tslist) - 1):
            ax.set_xticklabels(['ALL', 'ANT', 'GHG', 'AER', 'NAT'] + \
                               ['ANT', 'NAT'] + ['AER', 'ANTnoAER',
                                                 'NAT'] \
                               + ['GHG', 'ANTnoGHG', 'NAT'],
                               rotation = 90)
            for xtick in ax.get_xticklabels():
                xtick.set_color(clist[xtick.get_text()])
            ax.annotate('1-forcing', xy = (0.18, -0.5), xytext = (0.18, -0.65),
                        xycoords = 'axes fraction',
                        ha = 'center', va = 'bottom',
                        arrowprops=dict(arrowstyle='-[, widthB=4., ' + \
                                        'lengthB=0.6', lw=1.5))
            ax.annotate('2-forcing', xy = (0.45, -0.5), xytext = (0.45, -0.65),
                        xycoords = 'axes fraction',
                        ha = 'center', va = 'bottom',
                        arrowprops=dict(arrowstyle='-[, widthB=1.8, ' + \
                                        'lengthB=0.6', lw=1.5))
            ax.annotate('3-forcing',
                        xy = (0.65, -0.5), xytext = (0.65, -0.65),
                        xycoords = 'axes fraction',
                        ha = 'center', va = 'bottom',
                        arrowprops=dict(arrowstyle='-[, widthB=2.2, ' + \
                                        'lengthB=0.6', lw=1.5))
            ax.annotate('3-forcing', xy = (0.87, -0.5), xytext = (0.87, -0.65),
                        xycoords = 'axes fraction',
                        ha = 'center', va = 'bottom',
                        arrowprops=dict(arrowstyle='-[, widthB=2.2, ' + \
                                        'lengthB=0.6', lw=1.5))
        else:
            ax.set_xticklabels([])
        
        ax.set_ylabel(rg)
        if count == 0:
            ax.set_title('Scaling factor (-)')

        #######################################################################
        # 2. Attributable trend
        #######################################################################
        ax = axes[count, 1]
    
        h = []
        # Observation
        h.append(ax.bar(0.7, trend_all.loc[('Obs', 'Obs', 'hat', 'Obs', rg)],
                        color = [.6, .6, .6], width = 0.4))
        h.append(ax.bar(1.3, trend_all.loc[('Obs', 'Obs', 'hat', 'Avg', rg)],
                        color = [.4, .4, .4], width = 0.4))

        # One signal
        for sind, sig in enumerate(['historical', 'ANT', 'GHG', 'AER', 'NAT']):
            x = 3.5 + 2 * sind
            trend = trend_all.loc[('1-factor', sig, slice(None),
                                   slice(None), rg)]
            trend = trend.drop(['EC'], level = 1).unstack()
            trend = trend.T
            if trend.empty:
                continue

            beta = beta_all.loc[('1-factor', sig, slice(None),
                                 slice(None), rg)]
            beta.index = beta.index.droplevel([0,1,4])
            beta = beta.drop(['EC'], level = 1).unstack()
            beta = beta.T
            # skip the weird values
            trend.loc[(beta['hat'].values < beta['low'].values) | \
                      (beta['hat'].values > beta['up'].values), :] = np.nan
            #trend = trend.dropna(axis = 0, how = 'all')

            clist_sub = [clist[sig],
                         np.append(clist[sig][:3], 0.5)]

            htemp = ax.bar(x + np.array([-0.4,0.4]), trend['hat'].values,
                           color = clist_sub, width = 0.4)
            if sind == 0:
                h += htemp
            ax.errorbar(x + np.array([-0.4,0.4]), trend['hat'].values,
                        yerr = [trend['hat'].values - trend['low'].values,
                                trend['up'].values - trend['hat'].values], 
                        ecolor = 'k', elinewidth = 0.5, lw = 0)
    
        # Two signal
        for bind, bn in enumerate(['ANT', 'NAT']):
            x = 14.5 + 2 * bind
            trend = trend_all.loc[('2-factor', bn, slice(None),
                                   slice(None), rg)]
            trend = trend.drop(['EC'], level = 1).unstack()
            trend = trend.T
            if trend.empty:
                continue

            beta = beta_all.loc[('2-factor', bn, slice(None),
                                 slice(None), rg)]
            beta.index = beta.index.droplevel([0,1,4])
            beta = beta.drop(['EC'], level = 1).unstack()
            beta = beta.T
            # skip the weird values
            trend.loc[(beta['hat'].values < beta['low'].values) | \
                      (beta['hat'].values > beta['up'].values), :] = np.nan
            #trend = trend.dropna(axis = 0, how = 'all')
    
            clist_sub = [clist[bn], np.append(clist[bn][:3], 0.5)]

            ax.bar(x + np.array([-0.4,0.4]), trend['hat'].values,
                   color = clist_sub, width = 0.4)
            ax.errorbar(x + np.array([-0.4,0.4]), trend['hat'].values,
                        yerr = [trend['hat'].values - trend['low'].values,
                                trend['up'].values - trend['hat'].values],
                        ecolor = 'k', elinewidth = 0.5, lw = 0)
    
        # Three signal
        for bind, bn in enumerate(['AER', 'NoAER', 'NATaer']):
            x = 19.5 + 2 * bind
            trend = trend_all.loc[('3-factor', bn, slice(None),
                                   slice(None), rg)]
            trend = trend.drop(['EC'], level = 1).unstack()
            trend = trend.T
            if trend.empty:
                continue
    
            beta = beta_all.loc[('3-factor', bn, slice(None),
                                 slice(None), rg)]
            beta.index = beta.index.droplevel([0,1,4])
            beta = beta.drop(['EC'], level = 1).unstack()
            beta = beta.T
            # skip the weird values
            trend.loc[(beta['hat'].values < beta['low'].values) | \
                      (beta['hat'].values > beta['up'].values), :] = np.nan
            #trend = trend.dropna(axis = 0, how = 'all')

            clist_sub = [clist[bn], np.append(clist[bn][:3], 0.5)]

            ax.bar(x + np.array([-0.4,0.4]), trend['hat'].values,
                   color = clist_sub, width = 0.4)
            ax.errorbar(x + np.array([-0.4,0.4]), trend['hat'].values,
                        yerr = [trend['hat'].values - trend['low'].values,
                                trend['up'].values - trend['hat'].values],
                        ecolor = 'k', elinewidth = 0.5, lw = 0)
     
        for bind, bn in enumerate(['GHG', 'NoGHG', 'NATghg']):
            x = 26.5 + 2 * bind
            trend = trend_all.loc[('3-factor', bn, slice(None),
                                   slice(None), rg)]
            trend = trend.drop(['EC'], level = 1).unstack()
            trend = trend.T
            if trend.empty:
                continue
    
            beta = beta_all.loc[('3-factor', bn, slice(None),
                                 slice(None), rg)]
            beta.index = beta.index.droplevel([0,1,4])
            beta = beta.drop(['EC'], level = 1).unstack()
            beta = beta.T
            # skip the weird values
            trend.loc[(beta['hat'].values < beta['low'].values) | \
                      (beta['hat'].values > beta['up'].values), :] = np.nan
            #trend = trend.dropna(axis = 0, how = 'all')
    
            clist_sub = [clist[bn], np.append(clist[bn][:3], 0.5)]

            ax.bar(x + np.array([-0.4,0.4]), trend['hat'].values,
                   color = clist_sub, width = 0.4)
            ax.errorbar(x + np.array([-0.4,0.4]), trend['hat'].values,
                        yerr = [trend['hat'].values - trend['low'].values,
                                trend['up'].values - trend['hat'].values],
                        ecolor = 'k', elinewidth = 0.5, lw = 0)

        ax.set_xlim([0., 26.5 + 2*3])
        ax.set_ylim([-0.015, 0.015])
        ax.set_yticks(np.arange(-0.01, 0.013, 0.005))
        ax.axhline(0, ls = '--', color = 'grey', lw = 0.5)
        ax.axvline(2, ls = '--', color = 'k', lw = 0.5)
        ax.axvline(13, ls = '--', color = 'k', lw = 0.5)
        ax.axvline(18, ls = '--', color = 'k', lw = 0.5)
        ax.axvline(25, ls = '--', color = 'k', lw = 0.5)
        ax.text(-0.1, 0.95, lab[count*2 + 1], weight = 'bold',
                transform = ax.transAxes)
        ax.set_xticks([1.] + list(np.arange(3.5, 3.5 + 2 * 5, 2)) + \
                      list(np.arange(14.5, 14.5 + 4 * 1, 2)) + \
                      list(np.arange(19.5, 19.5 + 6 * 1, 2)) + \
                      list(np.arange(26.5, 26.5 + 6 * 1, 2)))
    
        if count == (len(tslist)-1):
            ax.set_xticklabels(['Obs'] + \
                               ['ALL', 'ANT', 'GHG', 'AER', 'NAT'] + \
                               ['ANT', 'NAT'] + ['AER', 'ANTnoAER',
                                                 'NAT'] \
                               + ['GHG', 'ANTnoGHG', 'NAT'],
                               rotation = 90)
            for xtick in ax.get_xticklabels():
                if xtick.get_text() != 'Obs':
                    xtick.set_color(clist[xtick.get_text()])
            ax.annotate('1-forcing', xy = (0.24, -0.5), xytext = (0.24, -0.65),
                        xycoords = 'axes fraction',
                        ha = 'center', va = 'bottom',
                        arrowprops=dict(arrowstyle='-[, widthB=3.7, ' + \
                                        'lengthB=0.6', lw=1.5))
            ax.annotate('2-forcing', xy = (0.485, -0.5),
                        xytext = (0.485, -0.65), xycoords = 'axes fraction',
                        ha = 'center', va = 'bottom',
                        arrowprops=dict(arrowstyle='-[, widthB=1.4, ' + \
                                        'lengthB=0.6', lw=1.5))
            ax.annotate('3-forcing', xy = (0.67, -0.5), xytext = (0.67, -0.65),
                        xycoords = 'axes fraction', ha = 'center',
                        va = 'bottom',
                        arrowprops=dict(arrowstyle='-[, widthB=2., ' + \
                                        'lengthB=0.6', lw=1.5))
            ax.annotate('3-forcing', xy = (0.88, -0.5), xytext = (0.88, -0.65),
                        xycoords = 'axes fraction', ha = 'center',
                        va = 'bottom',
                        arrowprops=dict(arrowstyle='-[, widthB=2., ' + \
                                        'lengthB=0.6', lw=1.5))
        else:
            ax.set_xticklabels([])
    
        if count == 0:
            ax.set_title('Attributable trend (year$^{-1}$)')

    ax.legend(h, ['AI_Merged', 'AI_NoEC',
                  'Uniform AI_Merged', 'Uniform AI_NoEC'],
              loc = [-0.9, -0.8], ncol = 4)

    fig.savefig(os.path.join(path_out, 'da2', method + '_ts_fig3_v3_2_' + \
                             var + '_' + aggr + '_' + str(tsmooth) + '_' + \
                             yrbrac_n + suffix + '.png'),
                dpi = 600., 
                bbox_inches = 'tight')
    plt.close(fig)
