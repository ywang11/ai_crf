import pandas as pd
import numpy as np
import statsmodels.api as stats
import itertools as it
import os
from .constants import olist, elist


def vector(beta_mat, RC_mat):
    result = np.full(RC_mat.shape[0], 5)

    j1 = (RC_mat[:, 1] >= RC_mat[:, 0]) & (RC_mat[:, 1] <= RC_mat[:, 2])
    result[~j1] = 0

    j2 = j1 & (beta_mat[:, 2] > 0)
    result[j1 & (~j2)] = 1

    j3 = j2 & (beta_mat[:, 0] > 0)
    result[j2 & (~j3)] = 2

    j4 = j3 & (beta_mat[:, 2] > 1)
    result[j3 & (~j4)] = 3

    j5 = j4 & (beta_mat[:, 0] > 1)
    result[j5] = 4

    return result


def judge(fname, expr, tsmooth, yrbrac_n):
    """
    Categories:
    0. Residual consistency test is not passed or the scaling factors cannot
       be solved.
    1. Scaling factor is significantly less than zero.
    2. Scaling factor is not significantly greater than zero.
    3. Scaling factor is significantly greater than zero and less than one.
    4. Scaling factor is sigificantly greater that zero, and above one.
    5. Scaling factor is significantly greater than zero and around one.
    """
    nbeta = len(expr.split('_'))

    y1 = int(yrbrac_n.split('-')[0])
    y2 = int(yrbrac_n.split('-')[1])
    if ((y2 - y1 + 1) // tsmooth) <= 3:
        if nbeta == 1:
            return np.nan
        else:
            return (np.nan,) * nbeta

    df = pd.read_csv(fname, index_col = 0)
    df = df.replace([np.inf, -np.inf], np.nan)
    df.dropna(inplace = True, axis = 0, how = 'any')
    if df.empty:
        if nbeta == 1:
            return 0
        else:
            return (0, ) * nbeta

    if nbeta == 1:
        return np.max(vector(df[['beta1_low','beta1_hat','beta1_up']].values,
                             df[['RClow','RCstat','RCup']].values))
    else:
        result = []
        for n in range(1, nbeta + 1):
            result.append(vector(df[['beta' + str(n) + '_low',
                                     'beta' + str(n) + '_hat',
                                     'beta' + str(n) + '_up']].values,
                                 df[['RClow', 'RCstat', 'RCup']].values))
        combine = np.asarray(result, order = 'F')
        combine_sum = np.sum(combine, axis = 0)
        if np.max(combine_sum) == 5 * nbeta:
            return (5,) * nbeta
        else:
            ind = np.argmax(combine_sum)
            return tuple(combine[:, ind])


def judge_beta(fname, expr, tsmooth, yrbrac_n):
    """
    Return the beta values for the highest scaling factor code.
    (beta1 (low, hat, up), beta2, ...)
    """
    nbeta = len(expr.split('_'))

    y1 = int(yrbrac_n.split('-')[0])
    y2 = int(yrbrac_n.split('-')[1])
    if ((y2 - y1 + 1) // tsmooth) <= 3:
        return np.array([np.nan, np.nan, np.nan] * nbeta)

    if not os.path.exists(fname):
        return np.array([np.nan, np.nan, np.nan] * nbeta)

    df = pd.read_csv(fname, index_col = 0)
    df = df.replace([np.inf, -np.inf], np.nan)
    df.dropna(inplace = True, axis = 0, how = 'any')
    if df.empty:
        return np.array([np.nan, np.nan, np.nan] * nbeta)
    #else:
    #    # No truncation
    #    beta = df[['beta' + str(n) + '_' + m for n in range(1, nbeta+1)
    #               for m in ['low', 'hat', 'up']]].values[-1, :]
    #    return beta

    # Truncation at the first significant number of EOF
    result = []
    for n in range(1, nbeta + 1):
        result.append(vector(df[['beta' + str(n) + '_low',
                                 'beta' + str(n) + '_hat',
                                 'beta' + str(n) + '_up']].values,
                             df[['RClow', 'RCstat', 'RCup']].values))
    combine = np.asarray(result, order = 'F')
    combine_sum = np.sum(combine, axis = 0)
    
    if int(max(combine_sum)) == 0: # residual inconsistent
        return np.array([np.nan, np.nan, np.nan] * nbeta)
    else:
        ind = np.argmax(combine_sum)
        beta = df[['beta' + str(n) + '_' + m for n in range(1, nbeta+1) \
                   for m in ['low', 'hat', 'up']]].values
        ## 2020/09/22 temporary: use the last ind, i.e. no truncation
        ##ind = len(combine_sum) - 1
        return beta[ind, :]


def attributed_trend(f_obs_sig, flist_beta, expr_list, tsmooth, yrbrac_n):
    """
    Following Mao et al. https://www.nature.com/articles/nclimate3056.pdf

    Attributable trends (that is, the trends explained by the external forcing
    under scrutiny) are derived by multiplying the model-simulated trend and 
    the estimated scaling factor. Similarly, upper and lower bounds of 
    attributable trends are derived from the corresponding upper and lower 
    bounds of β. 
    """
    obs_sig = pd.read_csv(f_obs_sig, sep = ',', header = None)
    # Must be consistent with ./constants.py
    obs_sig.index = olist + elist

    y1 = int(yrbrac_n.split('-')[0])
    y2 = int(yrbrac_n.split('-')[1])
    nt = ((y2 - y1 + 1) // tsmooth) # temporal dimension
    ns = obs_sig.shape[1] // nt # spatial dimension

    nbeta = len(expr_list[0].split('_'))

    trend = pd.DataFrame(data = np.nan, index = range(ns),
                         columns = pd.MultiIndex.from_product( \
                            [expr_list,
                             ['beta_' + str(n) for n in range(1, nbeta+1)],
                             ['low', 'hat', 'up']]))

    if nt <= 3:
        return trend

    # Calculate the trend in obs_sig
    obs_sig_trend = pd.DataFrame(np.nan, index = range(ns),
                                 columns = obs_sig.index)
    X = stats.add_constant(range(nt))
    for s,c in it.product(range(ns), obs_sig.index):
        mod = stats.OLS(obs_sig.loc[c, s::ns], X).fit()
        obs_sig_trend.loc[s, c] = mod.params[1]

    # Convert the signal trend using scaling factor
    for find, f_beta in enumerate(flist_beta):
        expr = expr_list[find]
        sig_list = [x.replace('AI-', '') for x in expr.split('_')]
        beta = judge_beta(f_beta, expr, tsmooth, yrbrac_n)

        #print(expr)
        #print(f_beta)
        #print(beta)
        for s in range(ns):
            tmp = obs_sig_trend.loc[s, sig_list].values
            trend.loc[s, (expr, slice(None))] = np.repeat(tmp, 3) * beta

    return trend, obs_sig_trend.loc[:, olist]
