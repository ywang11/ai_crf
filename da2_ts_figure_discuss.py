"""
20200413

Use scatter plots to plot the individual signals and Avg product's 
 relationships.
"""
import pandas as pd
from utils.constants import *
from utils.settings import *
import os
import itertools as it
from utils.da2 import judge
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.cm import get_cmap
import statsmodels.regression.linear_model as lm
from statsmodels.tools import add_constant
from scipy.stats import pearsonr


blist_n = [str(x[0]) + '-' + str(x[-1]) for x in blist]

aggr = 'TS'
var = 'AI'
tsmooth = '5' # '5', '6'
yrbrac_n = '1965-2014' # '1965-2014', '1948-2014'
obs = 'Avg'
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'


def plot_fit(ax, x, y, col, xy, **kwargs):
    h1, = ax.plot(x, y, 'o', color = col, **kwargs)
    
    mod = lm.OLS(y, add_constant(x))
    mod = mod.fit()
    slope = mod.params[1]
    intercept = mod.params[0]

    h2, = ax.plot([np.min(x), np.max(x)],
                 [intercept + slope * np.min(x),
                  intercept + slope * np.max(x)], '--', color = col)

    #if intercept < 0:
    #    symbol = '-'
    #    intercept = -intercept
    #else:
    #    symbol = '+'
    #if mod.pvalues[1] <= 0.05:
    #    sig_slope = '$^*$'
    #else:
    #    sig_slope = ''
    #if mod.pvalues[1] <= 0.05:
    #    sig_intercept = '$^*$'
    #else:
    #    sig_intercept = ''
    #ax.text(np.mean(x), intercept + slope * np.mean(x) - 0.5,
    #        ('y = %.2f' + sig_slope + ' $\cdot$ x ' + symbol + ' %.2f' + \
    #         sig_intercept) % (slope, intercept), color = col,
    #        fontdict = {'fontsize': 8, 'family' : 'sans',
    #                    'weight': 'normal'},
    #        transform = ax.transAxes)
    ax.text(xy[0], xy[1], ('slope = %.2f' % slope) + \
            ('*' if mod.pvalues[1] <= 0.1 else '') + \
            ('*' if mod.pvalues[1] <= 0.05 else ''), color = col)
    ##ax.annotate('R$^2$ = %.2f' % mod.rsquared,
    ##            xy = (np.min(x), np.max(y)),
    ##            arrowprops = {'width': 0})
    return h1, h2


cmap = get_cmap('nipy_spectral')
col = [cmap((i+0.7)/len(elist)) for i in range(len(elist))]

mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titleweight'] = 'normal'
fig, axes = plt.subplots(nrows = 3, ncols = 2, figsize = (6.5, 6.),
                         sharex = True, sharey = True)
fig.subplots_adjust(wspace = 0.03, hspace = 0.01)

run_name = ['All', '3-member']
h = []
for rind, run in enumerate(['run_1_all_ensemble_members',
                            'run_3_large_ensemble_members']):
    for gind, rg in enumerate(['Global', 'NH', 'SH']):
        ax = axes[gind, rind]

        data = pd.read_csv(os.path.join(path_out, 'da2', run, rg,
                                        'obs_sig', var + '_' \
                                        + aggr + '_' + tsmooth + '_' + \
                                        yrbrac_n + '.dat'), header = None)
        data.index = olist + elist

        for eind, e in enumerate(elist[:-1]):
            if eind <= 3:
                xy = (-0.02, 0.05 - 0.005 * eind)
            else:
                xy = (0.01, 0.045 - 0.005 * (eind-4))

            h1, h2 = plot_fit(ax, data.loc[e, :], data.loc[obs, :],
                              col = col[eind], xy = xy,
                              markersize = 3)
            h.append(h1)

        if gind == 0:
            ax.set_title(run_name[rind])
        if rind == 0:
            if gind == 1:
                ax.set_ylabel('Observed AI\n' + rg)
            else:
                ax.set_ylabel(rg)
        if (gind == 2) & (rind == 1):
            ax.text(-0.27, -0.2, 'Simulated AI', transform = ax.transAxes)
        ax.text(0.01,0.9,lab[gind*2+rind] + ')', transform = ax.transAxes,
                fontsize = 6)
ax.legend(h, ['ALL', 'ANT', 'GHG', 'AER', 'NAT', 'ANTNoAER', 'ANTNoGHG'],
          loc = (-1., -0.35), ncol = len(elist))
fig.savefig(os.path.join(path_out, 'da2', 'ts_fig3_discuss_' + \
                         var + '_' + aggr + '_' + str(tsmooth) + '_' + \
                         yrbrac_n + '_' + obs + '.png'), dpi = 600., 
            bbox_inches = 'tight')
plt.close(fig)
