"""
20200225

Average the observed AI and Arid area by EC, Obs, and all.
"""
import os
import pandas as pd
import itertools as it
import numpy as np
from utils.constants import alist_global, alist_7region
from utils.settings import *
from utils.da import *


olist = ['CRU-PRE', 'GPCC-PRE', 'UDEL-PRE', 'PREC-REC-PRE']
year_actual = {'CRU-PRE': '190101-201612',
               'GPCC-PRE': '190101-201612',
               'UDEL-PRE': '190101-201612', 
               'PREC-REC-PRE': '194801-201612'}
vlist = ['AI', 'Arid_1', 'Arid_2', 'Arid_3', 'Arid_4', 'Arid_5']


def avg_netcdf(flist, varname, which):
    if not which:
        for find, f in enumerate(flist):
            hr = xr.open_dataset(f)
            tmptmp = hr[varname].copy(deep = True) / len(flist)
            if '190101-201612' in f:
                tmptmp = tmptmp[47:, :, :]
            tmptmp['year'] = range(69)
            if find == 0:
                tmp = tmptmp
            else:
                tmp += tmptmp
            hr.close()
    else:
        which = int(which)
        for find, f in enumerate(flist):
            hr = xr.open_dataset(f)
            tmptmp = (hr[varname] == which).astype(int) / len(flist)
            if '190101-201612' in f:
                tmptmp = tmptmp[47:, :, :]
            tmptmp['year'] = range(69)
            if find == 0:
                tmp = tmptmp
                tmp = tmp.where(~np.isnan(hr[varname][0,:,:]), np.nan)
            else:
                tmp += tmptmp
            hr.close()

    return tmp


def avg_csv_white(flist):
    for find, f in enumerate(flist):
        if 'Lat' in f:
            df = pd.read_csv(f, sep = '\s+', na_values = -999)
            df.columns = ['%.2f' % float(x) for x in df.columns]
        else:
            df = pd.read_csv(f, header = None, sep = '\s+', na_values = -999)
        if '190101-201612' in f:
            df = df.iloc[47:, :].reset_index(drop = True)
        ##print(df)
        if find == 0:
            tmp = df / len(flist)
        else:
            tmp += df / len(flist)
    return tmp


def avg_csv(flist):
    for find, f in enumerate(flist):
        if 'Lat' in f:
            df = pd.read_csv(f)
            df.columns = ['%.2f' % float(x) for x in df.columns]
        else:
            df = pd.read_csv(f, header = None)
        if '190101-201612' in f:
            df = df.iloc[47:, :].reset_index(drop = True)
        #print(df)
        if find == 0:
            tmp = df / len(flist)
        else:
            tmp += df / len(flist)
    return tmp


###############################################################################
# Global
###############################################################################
for var, aggr, i in it.product(vlist, ['nc', 'lat', 'global', 'hemisphere'], 
                               ['Obs', 'EC']):
    if aggr == 'nc':
        flist = [os.path.join(path_data, 'CMIP6-AI', 'AI-' + i,
                              var.split('_')[0] + '_' + o + \
                              '_CRU-PET_' + year_actual[o] + '.nc') \
                 for o in olist]
        varname = var.split('_')[0].lower()
        if var == 'AI':
            tmp = avg_netcdf(flist, varname, '')
        else:
            tmp = avg_netcdf(flist, varname, var.split('_')[1])
        tmp.to_dataset(name = varname).to_netcdf(os.path.join( \
            path_out, 'obs_avg', i, var + '_' + aggr + '.nc'))
    else:
        if aggr == 'lat':
            flist = [os.path.join(path_data, 'CMIP6-AI', 'AI-' + i + '-Lat', 
                                  var, var.split('_')[0] + '_' + o + \
                                  '_CRU-PET_' + year_actual[o] + \
                                  '.csv') for o in olist]
            tmp = avg_csv_white(flist)
            tmp.to_csv(os.path.join(path_out, 'obs_avg', i + '-Lat',
                                    var + '_' + aggr + '.csv'),
                       index = False, sep = ',')
        elif aggr == 'global':
            flist = [os.path.join(path_data, 'CMIP6-AI', 'AI-' + i + '-TS',
                                  var, var.split('_')[0] + '_' + o + \
                                  '_CRU-PET_' + year_actual[o] + \
                                  '.csv') for o in olist]
            tmp = avg_csv_white(flist).iloc[:,0]
            tmp.to_csv(os.path.join(path_out, 'obs_avg', i + '-TS', 
                                    var + '_' + aggr + '.csv'),
                       index = False, header = False, sep = ',')
        elif aggr == 'hemisphere':
            flist = [os.path.join(path_data, 'CMIP6-AI', 'AI-' + i + '-TS',
                                  var, var.split('_')[0] + '_' + o + \
                                  '_CRU-PET_' + year_actual[o] + \
                                  '.csv') for o in olist]
            tmp = avg_csv_white(flist).iloc[:,1:]
            tmp.to_csv(os.path.join(path_out, 'obs_avg', i + '-TS', 
                                    var + '_' + aggr + '.csv'), 
                       index = False, header = False, sep = ',')


###############################################################################
# 7 regions
###############################################################################
for var, aggr, rg, i in it.product(vlist, alist_7region, r7list, 
                                   ['Obs', 'EC']):
    if aggr == 'lat':
        flist = [os.path.join(path_data, '7-Regions', rg,
                              'AI-' + i + '-Lat', var,
                              var + '_Annual_' + '-'.join(o.split('-')[:-1]) \
                              + '_' + i + '_' + year_actual[o] + '.csv') \
                 for o in olist]
        tmp = avg_csv_white(flist)
    else:
        df = pd.read_csv(os.path.join(path_data, '7-Regions', rg, 'AI-TS',
                                      var + '_Annual_' + i + '_r1i1p1f1_' + \
                                      '190101-201612.csv'), header = None,
                         sep = '\s+').iloc[47:, :].reset_index(drop = True)
        df2 = pd.read_csv(os.path.join(path_data, '7-Regions', rg, 'AI-TS',
                                       var + '_Annual_PREC_' + i + \
                                       '_r1i1p1f1_194801-201612.csv'),
                          header = None)
        tmp = pd.DataFrame(np.concatenate([df.values, df2.values],
                                          axis = 1).mean(axis = 1),
                           index = df.index, columns = [0])
    if aggr == 'lat':
        tmp.to_csv(os.path.join(path_out, 'obs_avg', i + '-Lat',
                                var + '_' + aggr + '_' + rg + '.csv'),
                   index = False, sep = ',')
    else:
        tmp.to_csv(os.path.join(path_out, 'obs_avg', i + '-TS', 
                                var + '_' + aggr + '_' + rg + '.csv'),
                   index = False, header = False, sep = ',')


###############################################################################
# (Obs + EC) / 2
###############################################################################
for var, aggr in it.product(vlist, ['nc', 'lat', 'global', 'hemisphere']):
    if aggr == 'nc':
        varname = var.split('_')[0].lower()
        tmp = avg_netcdf([os.path.join(path_out, 'obs_avg', 'Obs', 
                                       var + '_' + aggr + '.nc'),
                          os.path.join(path_out, 'obs_avg', 'EC', 
                                       var + '_' + aggr + '.nc')],
                         varname, '')
        tmp.to_dataset(name = varname).to_netcdf( \
            os.path.join(path_out, 'obs_avg', 'Avg', var + '_' + aggr + '.nc'))
    else:
        if aggr == 'lat':
            tmp = avg_csv([os.path.join(path_out, 'obs_avg', 'Obs-Lat',
                                        var + '_' + aggr + '.csv'),
                           os.path.join(path_out, 'obs_avg', 'EC-Lat',
                                        var + '_' + aggr + '.csv')])
            tmp.to_csv(os.path.join(path_out, 'obs_avg', 'Avg-Lat',
                                    var + '_' + aggr + '.csv'),
                       index = False, sep = ',')
        elif aggr == 'global':
            tmp = avg_csv([os.path.join(path_out, 'obs_avg', 'Obs-TS',
                                        var + '_' + aggr + '.csv'),
                           os.path.join(path_out, 'obs_avg', 'EC-TS',
                                        var + '_' + aggr + '.csv')])
            tmp.to_csv(os.path.join(path_out, 'obs_avg', 'Avg-TS',
                                    var + '_' + aggr + '.csv'),
                       index = False, header = False, sep = ',')
        elif (aggr == 'hemisphere'):
            tmp = avg_csv([os.path.join(path_out, 'obs_avg', 'Obs-TS',
                                        var + '_' + aggr + '.csv'),
                           os.path.join(path_out, 'obs_avg', 'EC-TS',
                                        var + '_' + aggr + '.csv')])
            tmp.to_csv(os.path.join(path_out, 'obs_avg', 'Avg-TS',
                                    var + '_' + aggr + '.csv'),
                       index = False, header = False, sep = ',')


for var, aggr, rg in it.product(vlist, alist_7region, r7list):
    if aggr == 'lat':
        tmp = avg_csv([os.path.join(path_out, 'obs_avg', 'Obs-Lat',
                                    var + '_' + aggr + '_' + rg + '.csv'),
                       os.path.join(path_out, 'obs_avg', 'EC-Lat',
                                    var + '_' + aggr + '_' + rg + '.csv')])
        tmp.to_csv(os.path.join(path_out, 'obs_avg', 'Avg-Lat',
                                var + '_' + aggr + '_' + rg + '.csv'),
                   index = False, sep = ',')
    else:
        tmp = avg_csv([os.path.join(path_out, 'obs_avg', 'Obs-TS',
                                    var + '_' + aggr + '_' + rg + '.csv'),
                       os.path.join(path_out, 'obs_avg', 'EC-TS',
                                    var + '_' + aggr + '_' + rg + '.csv')])
        tmp.to_csv(os.path.join(path_out, 'obs_avg', 'Avg-TS',
                                var + '_' + aggr + '_' + rg + '.csv'),
                   index = False, header = False, sep = ',')
