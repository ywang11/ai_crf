import statsmodels.api as sm
import numpy as np
from .settings import path_data, path_out
from .constants import r7list, r7lat1, r7lat2, r7lon1, r7lon2, \
    ts_model_id, olist_ts
import os
import sys
import xarray as xr
import pandas as pd
from glob import glob
import itertools as it


def ls_trend(vector):
    """
    Least squares trend, regardless whether significant.
    """
    model = sm.OLS(vector, sm.add_constant(np.arange(len(vector))))
    result = model.fit()
    return result.params[1]


def get_wgts0(nc = True, r = '', res = '0.5'):
    if nc:
        # Global area file
        hr = xr.open_dataset(os.path.join(path_data,
                                          'elm_half_degree_grid_negLon.nc'))
        # Scale of the weights does not matter
        wgts0 = np.sqrt(hr['area'].values.copy()) 
        hr.close()
    else:
        wgts0 = np.sqrt(pd.read_csv(os.path.join(path_out,
                                                 'bylat_area_' + res + '.csv'),
                                    index_col = 0 \
                                ).loc[:, r])
        wgts0.index = ['%.2f' % float(x) for x in wgts0.index]

    return wgts0


def calc_signal(eof1, data_array, wgts, L):
    """
    Obtain the linear trend signal of data_array using eof1.
    Weights are always normalized to nanmean = 1 in the data_array mask.
    """
    # Project the time series unto the EOF with weights.
    if len(data_array.shape) == 2:
        wgts[np.isnan(data_array[0, :])] = np.nan
        wgts = wgts / np.nanmean(wgts)
        pc = np.nanmean(data_array * eof1 * wgts, axis = 1)
    else:
        wgts[np.isnan(data_array[0, :, :])] = np.nan
        wgts = wgts / np.nanmean(wgts)
        pc = np.nanmean(np.nanmean(data_array * eof1 * wgts, axis = 2),
                        axis = 1)

    # Obtain the least squares trend time series.
    trend = np.full(data_array.shape[0], np.nan)
    for t in range(L, data_array.shape[0] + 1):
        trend[t-1] = ls_trend(pc[(t-L):t])

    return trend


def by_hemisphere_mask():
    """
    Return a dictionary of the boolean mask of each hemisphere.
    """
    res = 0.5
    lat = np.arange(-90 + res/2, 90.1 - res/2, res)
    lon = np.arange(-180 + res/2, 180.1 - res/2, res)
    _, lat2D = np.meshgrid(lon, lat)
    return {'NH': xr.DataArray(lat2D > 0., dims = ['lat', 'lon'],
                               coords = {'lat': lat, 'lon': lon}),
            'SH': xr.DataArray(lat2D < 0., dims = ['lat', 'lon'], 
                               coords = {'lat': lat, 'lon': lon})}

def by_7region_mask(r):
    res = 0.5
    lat = np.arange(-90 + res/2, 90.1 - res/2, res)
    lon = np.arange(-180 + res/2, 180.1 - res/2, res)
    lon2D, lat2D = np.meshgrid(lon, lat)

    rind = np.where([x == r for x in r7list])[0][0]
    #print(rind)

    return xr.DataArray((lat2D >= r7lat1[rind]) & (lat2D < r7lat2[rind]) & \
                        (lon2D >= r7lon1[rind]) & (lon2D < r7lon2[rind]),
                        dims = ['lat', 'lon'],
                        coords = {'lat': lat, 'lon': lon})

def lat_mask(lat):
    # Mask high-latitude areas.
    # Right now: 
    # remove > 63N, even though the fingerprints are not reasonable >55N
    #      remove 55.75S because PREC and UDEL do not have value.
    # return (lat < 65) & (lat > -55.75)
    return (lat < 70) & (lat > -55.74)


def read_f(f, aggr, r, skip_lat5 = False, skip_hemi = False):
    # sep = r'[\*\s]+', engine = 'python'
    if aggr == 'lat':
        df = pd.read_csv(f, sep = r'\s+|,', engine = 'python')
        df.columns = ['%.2f' % float(x) for x in df.columns]
    else:
        df = pd.read_csv(f, sep = r'\s+|,', engine = 'python', header = None)

    df[np.abs(df + 999) < 1e-4] = np.nan

    if (aggr == 'lat') & ('Oceania' in f) & (not 'UDEL' in f):
        # ---- remove the -10.75 latitude because UDEL does not have it.
        df.loc[:, '-10.75'] = np.nan

    if aggr == 'lat':
        lat = np.array([float(x) for x in df.columns.values])
        if r == 'NH':
            subset = lat_mask(lat) & (lat > 0.)
        elif r == 'SH':
            subset = lat_mask(lat) & (lat < 0.)
        else:
            subset = lat_mask(lat)
        lat = lat[subset]
        df = df.loc[:, subset]
    elif aggr == 'ts':
        if skip_hemi:
            df = df
        else:
            if r == 'global':
                df = df.iloc[:, [0]]
            elif r == 'NH':
                if df.shape[1] > 2:
                    df = df.iloc[:, [2]]
                else:
                    df = df.iloc[:, [1]]
            elif r == 'SH':
                if df.shape[1] > 2:
                    df = df.iloc[:, [1]]
                else:
                    df = df.iloc[:, [0]]
            else:
                df = df # contains multiple models

    # Aggregate df to 5-degree bands, using land area weights.
    if (not skip_lat5) and (aggr == 'lat'):
        # ---- remove NaN columns. Need for Oceania, which has gaps.
        df = df.dropna(axis = 1, how = 'all')
        lat = np.array([float(x) for x in df.columns.values])

        lat_area = pd.read_csv(os.path.join(path_out, 'bylat_area_0.5.csv'),
                               index_col = 0)
        lat_area.index = ['%.2f' % x for x in lat_area.index]

        lat_area = lat_area.loc[df.columns, r]

        #
        lat0 = np.arange(-60, 71, 5)
        lat0 = lat0[(lat0 >= np.min(lat)) & (lat0 <= (np.max(lat)+5))]

        #
        df2 = pd.DataFrame(np.nan, index = df.index, 
                           columns = ['%.2f' % (x+2.5) for x in 
                                      lat0[:-1]])
        for l in range(len(lat0)-1):
            tmp = (lat >= lat0[l]) & (lat < lat0[l+1])
            if sum(tmp) < 1e-3:
                continue
            df2.iloc[:, l] = np.average(df.loc[:, tmp], axis = 1,
                                        weights = lat_area[tmp])

        # ---- second round removal of NaN columns.
        df = df2.dropna(axis = 1, how = 'all')

    return df


def read_ts(yrstr, var, rg, expr, aggr, files, remove_list = []):
    if expr == 'piControl':
        flist = glob(os.path.join(path_data, 'TimeSeries-CO2', yrstr, rg,
                                  var + '_' + aggr + '_' + expr + '_' + \
                                  rg + '_*_0001-0200.csv'))
    elif expr == 'Observation':
        flist = glob(os.path.join(path_data, 'TimeSeries-CO2', yrstr, rg,
                                  var + '_' + aggr + '_' + expr + '_' + \
                                  rg + '_' + yrstr + '.csv'))
    else:
        if (files == 'all') | (files == 'large') | \
           (files == 'uniform_all') | (files == 'uniform_large') | \
           (files == 'limited_all') | (files == 'limited_large'):
            # All the model ensemble members
            flist = glob(os.path.join(path_data, 'TimeSeries-CO2', yrstr, rg, 
                                      var + '_' + aggr + '_' + expr + '_' + \
                                      rg + '*' + yrstr + '.csv'))
        elif files == 'one':
            # Only the first ensemble member of each model
            flist = glob(os.path.join(path_data, 'TimeSeries-CO2', yrstr, rg,
                                      var + '_' + aggr + '_' + expr + '_' + \
                                      rg + '_r1i1p1f1_' + yrstr + '.csv'))
            
    for find, f in enumerate(flist):
        ##print(f)
        ensemble_id = f.split('_')[-2]

        tmp = pd.read_csv(f, sep = '\s+', header = None)
        if expr == 'Observation':
            tmp.columns = olist_ts
        else:
            ##print(ensemble_id, expr)
            tmp.columns = ts_model_id[ensemble_id][expr]
        ##print(tmp.columns)

        if expr != 'Observation':
            if files == 'limited_all':
                sset = {}
                for bb in [expr, 'piControl']:
                    sset[bb] = set(ts_model_id['r1i1p1f1'][bb]) | \
                        set(ts_model_id['r2i1p1f1'][bb]) | \
                        set(ts_model_id['r3i1p1f1'][bb])
                overlap_models = set(sset[expr]) & set(sset['piControl'])
                tmp = tmp.loc[:, sorted(overlap_models & set(tmp.columns))]
            elif files == 'limited_large':
                overlap_models = set(ts_model_id['r1i1p1f1']['historical'])
                for aa, bb in it.product(['r1i1p1f1', 'r2i1p1f1', 'r3i1p1f1'],
                                         [expr, 'piControl']):
                    if (aa != 'r1i1p1f1') & (bb == 'piControl'):
                        continue
                    overlap_models = overlap_models & set(ts_model_id[aa][bb])
                tmp = tmp.loc[:, sorted(overlap_models)]
            elif (files == 'uniform_all') & (expr != 'piControl'):
                sset = {}
                for bb in ['historical', 'GHG', 'NAT', 'AER']: #  'piControl'
                    sset[bb] = set(ts_model_id['r1i1p1f1'][bb]) | \
                        set(ts_model_id['r2i1p1f1'][bb]) | \
                        set(ts_model_id['r3i1p1f1'][bb])
                overlap_models = set(sset['historical'])
                for bb in ['GHG', 'NAT', 'AER']: #  'piControl'
                    overlap_models = overlap_models & set(sset[bb])
                tmp = tmp.loc[:, sorted(overlap_models & set(tmp.columns))]
            elif (files == 'uniform_large') & (expr != 'piControl'):
                overlap_models = set(ts_model_id['r1i1p1f1']['historical'])
                for aa, bb in it.product(['r1i1p1f1', 'r2i1p1f1', 'r3i1p1f1'],
                                         ['historical','GHG','NAT','AER' \
                                          ]): # 'piControl'
                    if (aa != 'r1i1p1f1') & (bb == 'piControl'):
                        continue
                    overlap_models = overlap_models & set(ts_model_id[aa][bb])
                ##print(overlap_models)
                tmp = tmp.loc[:, sorted(overlap_models)]
            elif (files == 'large') & (expr != 'piControl'):
                # Only keep the models that have three members.
                overlap_models = set(ts_model_id['r1i1p1f1'][expr]) & \
                    set(ts_model_id['r2i1p1f1'][expr]) & \
                    set(ts_model_id['r3i1p1f1'][expr])
                tmp = tmp.loc[:, sorted(overlap_models)]
        ##print(expr, files, tmp.columns)

        ##print(remove_list)
        ##print(tmp.columns)
        
        for mod in remove_list:
            if mod in tmp.columns:
                ##print(mod)
                tmp.drop(mod, axis = 1, inplace = True)

        if find == 0:
            df = tmp
        else:
            df = pd.concat([df, tmp], axis = 1)
    return df
