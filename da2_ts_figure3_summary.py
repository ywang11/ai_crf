"""
20200929

Create a table to summarize whether the scaling factors is above 1
 and covers 1.
Dependency: "da2_ts_figure3_data.py", ols & tls results, different ensemble
  members.
"""
import pandas as pd
from utils.constants import *
from utils.settings import *
import os
import itertools as it
from utils.da2 import judge
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from utils.plotting import mergecells
import matplotlib as mpl
from matplotlib.cm import get_cmap


aggr = 'TS'
var = 'AI'
tsmooth = '5' # '5', '6'
yrbrac_n = '1965-2014' # '1965-2014', '1948-2014'
suffix = '_single_noise'


#
##signal_list = {'1-factor': ['historical', 'ANT', 'GHG', 'AER', 'NAT'],
##               '2-factor': ['ANT', 'NAT'],
##               '3-factor': ['AER', 'NoAER', 'NATaer',
##                            'GHG', 'NoGHG', 'NATghg']}
signal_list = ['historical', 'ANT', 'GHG', 'AER', 'NAT',
               'NoAER', 'NoGHG']
signal_list_name = {'historical': 'ALL',
                    'ANT': 'ANT', 'GHG': 'GHG', 'NAT': 'NAT',
                    'AER': 'AER', # 'NATaer': 'NAT', 'NATghg': 'NAT',
                    'NoAER': 'ANTNoAER',
                    'NoGHG': 'ANTNoGHG'}
abbrev_list = ['DA', 'D', 'N']
region_list = ['Global', 'NH', 'SH']
method_list = ['ols', 'tls']
##n_pass = pd.DataFrame(data = 0,
##                      index = pd.MultiIndex.from_tuples([(mm, rr, ss,
##                          signal_list_name[ff]) for mm in method_list \
##                     for rr in region_list for ss in signal_list.keys() \
##                          for ff in signal_list[ss]]),
##                      columns = pd.MultiIndex.from_product([abbrev_list,
##                                                            olist]))
n_pass = pd.DataFrame(data = 0,
                      index = pd.MultiIndex.from_tuples([(mm, rr,
                        signal_list_name[ff]) for mm in method_list \
                        for rr in region_list for ff in signal_list]),
                      columns = pd.MultiIndex.from_product([olist,
                                                            abbrev_list]))


# Rating
# 1 - the CI is above 0, and covers 1
# 2 - the CI is above 1
# 3 - otherwise
def judge(sf):
    sf_judge = np.full(sf.shape[1], 0, dtype = int)
    for j in range(sf.shape[1]):
        if sf.loc['low', :].values[j] > 1:
            sf_judge[j] = 2
        elif (sf.loc['up', :].values[j] >= 1) & \
             (sf.loc['low', :].values[j] <= 1) & \
             (sf.loc['low', :].values[j] > 0):
            sf_judge[j] = 1
        else:
            sf_judge[j] = 3
    return sf_judge


for method, rg, obs in it.product(method_list, region_list, olist):
    beta_all = pd.concat([pd.read_csv(os.path.join(path_out, 'da2',
        'run_1_all_ensemble_members', method + '_ts_fig3_sf_' + var + '_' + \
        aggr + '_' + tsmooth + '_' + yrbrac_n + suffix + '.csv'),
                                      index_col = [0, 1, 2], header = [0, 1] \
                                      ).stack().stack(),
               pd.read_csv(os.path.join(path_out, 'da2',
        'run_4_all_ensemble_members', method + '_ts_fig3_sf_' + var + '_' + \
        aggr + '_' + tsmooth + '_' + yrbrac_n + suffix + '.csv'),
                           index_col = [0, 1, 2], header = [0, 1] \
                           ).stack().stack(),
                pd.read_csv(os.path.join(path_out, 'da2',
        'run_3_large_ensemble_members', method + '_ts_fig3_sf_' + var + '_' + \
        aggr + '_' + tsmooth + '_' + yrbrac_n + suffix + '.csv'),
                            index_col = [0, 1, 2], header = [0, 1] \
                            ).stack().stack(),
                pd.read_csv(os.path.join(path_out, 'da2',
        'run_6_all_ensemble_members', method + '_ts_fig3_sf_' + var + '_' + \
        aggr + '_' + tsmooth + '_' + yrbrac_n + suffix + '.csv'),
                         index_col = [0, 1, 2], header = [0, 1] \
                         ).stack().stack()], axis = 1)
    beta_all.columns = ['All', 'Limited', '3-member', 'Uniform']

    #
    for ss in signal_list:
        ##beta = beta_all.loc[(ss, ff, slice(None), obs, rg), :]
        beta = beta_all.loc[(slice(None), ss, slice(None), obs, rg), :]
        beta.index = beta.index.droplevel([1,3,4])
        beta = beta.reorder_levels([1,0]).sort_index().unstack()
        
        #
        sf_judge = judge(beta)
    
        #
        ar, counts = np.unique(sf_judge, return_counts = True)

        #
        for aind, aa in enumerate(ar):
            n_pass.loc[(method, rg, signal_list_name[ss]),
                       (obs, abbrev_list[aa-1])] += counts[aind]

# Convert to percentages
n_pass_temp = n_pass.groupby(n_pass.columns.get_level_values(0),
                             axis = 1).sum()
for col in n_pass_temp.columns:
    for j in abbrev_list:
        n_pass.loc[:, (col, j)] = n_pass.loc[:, (col, j)] / n_pass_temp[col]
n_pass.to_csv(os.path.join(path_out_da(path_out, 'da2'),
                           'n_pass' + suffix + '.csv'))
