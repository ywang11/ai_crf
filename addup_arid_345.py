"""
20200225

Add up the Arid_3, Arid_4, and Arid_5 results.
"""
import os
import pandas as pd
import itertools as it
import numpy as np
from utils.constants import vlist, alist_global, alist_7region, olist
from utils.settings import *
from utils.da import *
import xarray as xr


def add_netcdf(f):
    hr = xr.open_dataset(f)

    tmp = xr.DataArray(np.full(hr['arid'].shape, np.nan), 
                       dims = ['year', 'lat', 'lon'],
                       coords = {'year': hr['year'],
                                 'lat': hr['lat'], 'lon': hr['lon']})
    tmp = tmp.where(~((hr['arid'].values == 2) | (hr['arid'].values == 3) | \
                      (hr['arid'].values == 4)), 1)

    hr.close()
    return tmp


def add_netcdf3(flist):
    for ind, f in enumerate(flist):
        hr = xr.open_dataset(f)
        if ind == 0:
            tmp = hr['arid'].values.copy()
        else:
            tmp += hr['arid'].values.copy()
        hr.close()
    tmp = xr.DataArray(tmp, dims = ['year', 'lat', 'lon'],
                       coords = {'year': hr['year'],
                                 'lat': hr['lat'], 'lon': hr['lon']})
    return tmp


def add_csv_white(flist):
    for find, f in enumerate(flist):
        if 'Lat' in f:
            df = pd.read_csv(f, sep = '\s+', na_values = -999)
            df.columns = ['%.2f' % float(x) for x in df.columns]
        else:
            df = pd.read_csv(f, header = None, sep = '\s+', na_values = -999)
        ##print(df)
        if find == 0:
            tmp = df
        else:
            tmp += df
    return tmp


def add_csv(flist):
    for find, f in enumerate(flist):
        if 'Lat' in f:
            df = pd.read_csv(f)
            df.columns = ['%.2f' % float(x) for x in df.columns]
        else:
            df = pd.read_csv(f, header = None)
        #print(df)
        if find == 0:
            tmp = df
        else:
            tmp += df
    return tmp


###############################################################################
# Signal and piControl
###############################################################################
# Global
folder = sorted(glob(os.path.join(path_data, 'CMIP6-AI', 'AI*')))
folder = [fd for fd in folder if not 'Obs' in fd]
for fd in folder:
    path_out_fd = os.path.join(path_out, 'Arid_234', 'CMIP6-AI', 
                               fd.split('/')[-1])

    if ('Lat' in fd) | ('TS' in fd):
        flist = {}
        for i in [2, 3, 4]:
            flist[i] = sorted(glob(os.path.join(path_data, 'CMIP6-AI', fd, 
                                                'Arid_' + str(i), '*')))
        for i in range(len(flist[2])):
            tmp = add_csv_white([flist[2][i], flist[3][i], flist[4][i]])
            if 'TS' in fd:
                tmp.to_csv(os.path.join(path_out_fd,
                    flist[2][i].split('/')[-1].replace('Arid_2','Arid')), 
                           index = False, header = False)
            else:
                tmp.to_csv(os.path.join(path_out_fd,
                    flist[2][i].split('/')[-1].replace('Arid_2','Arid')), 
                           index = False)
    else:
        flist = glob(os.path.join(path_data, 'CMIP6-AI', fd, 'Arid*'))
        for f in flist:
            f234 = add_netcdf(f)
            f234.to_dataset(name = 'arid').to_netcdf(os.path.join(path_out_fd,
                f.split('/')[-1]))


# 7 regions
for rg in r7list:
    folder = sorted(glob(os.path.join(path_data, '7-Regions', rg, 'AI*')))
    folder = [fd for fd in folder if not 'Obs' in fd]
    for fd in folder:
        path_out_fd = os.path.join(path_out, 'Arid_234', '7-Regions', rg, 
                                   fd.split('/')[-1])

        if 'Lat' in fd:
            flist = {}
            for i in [2, 3, 4]:
                flist[i] = sorted(glob(os.path.join(path_data, '7-Regions',
                                                    rg, fd, 'Arid_' + str(i),
                                                    '*.csv')))
            for i in range(len(flist[2])):
                tmp = add_csv_white([flist[2][i], flist[3][i], flist[4][i]])
                tmp.to_csv(os.path.join(path_out_fd, 
                    flist[2][i].split('/')[-1].replace('Arid_2', 'Arid')),
                           index = False)
        elif 'TS' in fd:
            flist = sorted(glob(os.path.join(path_data, '7-Regions', rg, fd, 
                                             'Arid_2_Annual_*.csv')))
            for f in flist:
                tmp = add_csv_white([f, f.replace('Arid_2', 'Arid_3'),
                                     f.replace('Arid_2', 'Arid_4')])
                tmp.to_csv(os.path.join(path_out_fd,
                                        f.split('/')[-1].replace('Arid_2',
                                                                 'Arid')),
                           index = False, header = False)


###############################################################################
# Observation
###############################################################################
# NetCDF
for obs in olist:
    flist = []
    for i in [2, 3, 4]:
        flist.append(os.path.join(path_out, 'obs_avg', obs, 
                                  'Arid_' + str(i) + '_nc.nc'))
    tmp = add_netcdf3(flist)
    tmp /= len(flist)
    tmp.to_dataset(name = 'arid').to_netcdf(os.path.join(path_out, 'Arid_234',
        'obs_avg', obs, 'Arid_234_nc.nc'))


# Lat/TS
for obs, aggr in it.product(olist, ['Lat', 'TS']):
    flist = {}
    for i in [2, 3, 4]:
        flist[i] = sorted(glob(os.path.join(path_out, 'obs_avg',
                                            obs + '-' + aggr,
                                            'Arid_' + str(i) + '*.csv')))
    for i in range(len(flist[2])):
        tmp = add_csv([flist[2][i], flist[3][i], flist[4][i]])
        if aggr == 'TS':
            tmp.to_csv(os.path.join(path_out, 'Arid_234', 'obs_avg', 
                                    obs + '-' + aggr,
                flist[2][i].split('/')[-1].replace('Arid_2',
                                                   'Arid_234')),
                       index = False, header = False)
        else:
            tmp.to_csv(os.path.join(path_out, 'Arid_234', 'obs_avg', 
                                    obs + '-' + aggr,
                flist[2][i].split('/')[-1].replace('Arid_2',
                                                   'Arid_234')), index = False)
