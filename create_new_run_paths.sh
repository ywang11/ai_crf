#
mkdir run_1_all_ensemble_members run_3_large_ensemble_members
for i in run_1_all_ensemble_members run_3_large_ensemble_members
do
    mkdir ${i}/Global ${i}/NH ${i}/SH
    for j in Global NH SH
    do
	cd ${i}/${j}/
	mkdir obs_sig noise1 noise2 trend graph
	cd ../../
    done
done


#
mkdir run_4_all_ensemble_members run_5_large_ensemble_members
for i in run_4_all_ensemble_members run_5_large_ensemble_members
do
    mkdir ${i}/Global ${i}/NH ${i}/SH
    for j in Global NH SH
    do
	cd ${i}/${j}/
	mkdir obs_sig noise1 noise2 trend graph
	cd ../../
    done
done

#
mkdir run_6_all_ensemble_members run_7_large_ensemble_members
for i in run_6_all_ensemble_members run_7_large_ensemble_members
do
    mkdir ${i}/Global ${i}/NH ${i}/SH
    for j in Global NH SH
    do
	cd ${i}/${j}/
	mkdir obs_sig noise1 noise2 trend graph
	cd ../../
    done
done


#
mkdir run_2_one_ensemble_member
for i in run_2_one_ensemble_member
do
    mkdir ${i}/Global ${i}/NH ${i}/SH
    for j in Global NH SH
    do
        cd ${i}/${j}/
        mkdir obs_sig noise1 noise2 trend graph
        cd ../../
    done
done
