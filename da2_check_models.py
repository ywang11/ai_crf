"""
20210413

Check which models are in different ensemble setups.

Found uniform_all and uniform_large to have the same ensemble members,
 all and large have the same ensemble members except in the historical
 experiment, limited_all and limited_large have the same ensemble members
 except in the historical experiment.
"""
from utils.settings import *
from utils.constants import *
from utils.da import *
import os
import pandas as pd
import itertools as it
import numpy as np
import multiprocessing as mp
import warnings
warnings.filterwarnings("error")


# modify
remove_list = []                              # 'bcc-csm2-mr', 'cesm2',
                                              # 'cesm2-waccm', 'noresm2-lm'
                                              # 'canesm5', 'ec-earth3-veg'
                                              # 'inm-cm4-8'

en_list =['all', 'one', 'large', 'uniform_all', 'uniform_large',
          'limited_all', 'limited_large']

var = vlist[0]
rg = tslist[0]
aggr = 'TS'
tsmooth = tlist[0]
yrbrac = blist[0]
if yrbrac[0] == 1949:
    yrstr = '1948-2014'
else:
    yrstr = str(yrbrac[0]) + '-' + str(yrbrac[-1])


collection = pd.DataFrame(0, index = ts_model_id['r1i1p1f1']['historical'],
                          columns = pd.MultiIndex.from_product([en_list,
                                                                elist]))
for ensemble in en_list:
    for e in elist:
        ##print(e)
        df = read_ts(yrstr, var, rg, e, aggr, ensemble, remove_list)
        if yrbrac[0] == 1949:
            df = df.loc[1:, :]
        df.index = yrbrac

        for col in df.columns:
            print(e, col)
            collection.loc[col, (ensemble, e)] += 1
collection.to_csv(os.path.join(path_out, 'da2', 'collection.csv'))
