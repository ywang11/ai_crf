# -*- coding: utf-8 -*-
"""
20190910

@author: ywang254
"""
import numpy as np
import os
import sys
import matplotlib.pyplot as plt
from matplotlib import colors
from matplotlib import cm
import cartopy.crs as ccrs
from cartopy.util import add_cyclic_point
import xarray as xr
import matplotlib as mpl


def plot_ts_shade(ax, time, matrix, ts_label = '', ts_col = 'red',
                  shade_col = 'red', alpha = 0.2, skipna = False):
    """
    Plot a shaded ensemble.
    """
    if skipna:
        ts_min = np.nanmin(matrix, axis = 1)
        ts_mean = np.nanmean(matrix, axis = 1)
        ts_max = np.nanmax(matrix, axis = 1)
    else:
        ts_min = np.min(matrix, axis = 1)
        ts_mean = np.mean(matrix, axis = 1)
        ts_max = np.max(matrix, axis = 1)

    hl, = ax.plot(time, ts_mean, '-', color = ts_col, linewidth = 2,
                  label = ts_label)
    ax.plot(time, ts_min, '--', color = shade_col, linewidth = 1)
    ax.plot(time, ts_max, '--', color = shade_col, linewidth = 1)
    ax.fill_between(time, ts_min, ts_max, where = ts_max > ts_min,
                    facecolor = shade_col, alpha = alpha)
    hfill, = ax.fill(np.nan, np.nan, facecolor = shade_col, alpha = alpha)
    return hl, hfill


def plot_ts_trend(ax, time, ts, txt_x, txt_y,
                  ts_kw = {'color': 'k', 'marker': '.', 'markersize': 3.},
                  ln_kw = {'color': 'k', 'linestyle': '-', 'linewidth': 1.5}):
    """
    Plot the time series with trend.
    """
    h1, = ax.plot(time, ts, **ts_kw)

    model = lm.OLS(ts, add_constant(np.arange(0, len(time))))
    result = model.fit()
    params_CI = result.conf_int()

    """
    ax.text(txt_x, txt_y + 0.025, 'Slope = %.2E (%.2E, %.2E)'  % \
            (result.params[1], params_CI[1,0], params_CI[1,1]),
            transform = ax.transAxes, color = ln_kw['color'],
            fontsize = 8)
    ax.text(txt_x, txt_y - 0.025, 'Intercept = %.2E (%.2E, %.2E)' % \
            (result.params[0], params_CI[0,0], params_CI[0,1]),
            transform = ax.transAxes, color = ln_kw['color'],
            fontsize = 8)
    """

    slope = result.params[1]
    intercept = result.params[0]

    h2, = ax.plot(time, np.arange(0, len(time)) * slope + intercept,
                  **ln_kw)

    if intercept < 0:
        symbol = '-'
        intercept = -intercept
    else:
        symbol = '+'
    format_str = 'y = %.6f * x ' + symbol + ' %.6f (%.3f, %.3f)'

    ax.text(txt_x, txt_y, format_str % \
            (slope, intercept, result.pvalues[1], result.pvalues[0]),
            transform = ax.transAxes, color = ln_kw['color'],
            fontdict = {'fontsize': 8, 'family' : 'serif',
                        'weight': 'bold'})
    ##print(np.arange(0, len(time)) * slope + intercept)
    return h1, h2


def cmap_gen(cmap_1, cmap_2):
    """
    Generate a new colormap by stacking two color maps.
    """
    c1 = cm.get_cmap(cmap_1, 128)
    c2 = cm.get_cmap(cmap_2, 128)
    new1 = c1(np.linspace(0, 1, 128))
    new2 = c2(np.linspace(0, 1, 128))
    newcmp = colors.ListedColormap(np.concatenate([new1, new2], axis = 0))
    return newcmp


def mergecells(table, ix0, ix1):
    """
    Merge cells in matplotlib table
    https://stackoverflow.com/questions/53783087/double-header-in-matplotlib-table
    """
    ix0,ix1 = np.asarray(ix0), np.asarray(ix1)
    d = ix1 - ix0
    if not (0 in d and 1 in np.abs(d)):
        raise ValueError("ix0 and ix1 should be the indices of adjacent cells. ix0: %s, ix1: %s" % (ix0, ix1))

    if d[0]==-1:
        edges = ('BRL', 'TRL')
    elif d[0]==1:
        edges = ('TRL', 'BRL')
    elif d[1]==-1:
        edges = ('BTR', 'BTL')
    else:
        edges = ('BTL', 'BTR')

    # hide the merged edges
    for ix,e in zip((ix0, ix1), edges):
        table[ix[0], ix[1]].visible_edges = e

    txts = [table[ix[0], ix[1]].get_text() for ix in (ix0, ix1)]
    tpos = [np.array(t.get_position()) for t in txts]


    # center the text of the 0th cell between the two merged cells
    ##trans = (tpos[1] - tpos[0])/2
    ##if trans[0] > 0 and txts[0].get_ha() == 'right':
    ##    # reduce the transform distance in order to center the text
    ##    trans[0] /= 2
    ##elif trans[0] < 0 and txts[0].get_ha() == 'right':
    ##    # increase the transform distance...
    ##    trans[0] *= 2
    ##
    ##txts[0].set_transform(mpl.transforms.Affine2D().translate(*trans))
    tmp = table.properties()["celld"]
    tmp[tuple(ix0)]._loc = 'left'

    table._cells[tuple(ix1)].set_facecolor([0,0,0,0])

    # hide the text in the 1st cell
    txts[1].set_visible(False)


def mergecells4(table, ix_list):
    """
    Merge cells in matplotlib table
    https://stackoverflow.com/questions/53783087/double-header-in-matplotlib-table
    """
    ix_list = [np.asarray(ix_list[i]) for i in range(len(ix_list))]

    for i in range(len(ix_list)-1):
        d = ix_list[i+1] - ix_list[i]

        if not (0 in d and 1 in np.abs(d)):
            raise ValueError("ix0 and ix1 should be the indices of adjacent cells. ix0: %s, ix1: %s" % (ix_list[i], ix_list[i+1]))

        if i == 0:
            edge = 'BTL'
        ##elif i == (len(ix_list)-2):
        ##    edge = 'BTR'
        else:
            edge = 'BT'

        # hide the merged edges
        ix = ix_list[i]
        table[ix[0], ix[1]].visible_edges = edge
    table[ix_list[-1][0], ix_list[-1][1]].visible_edges = 'BTR'

    txts = [table[ix[0], ix[1]].get_text() for ix in ix_list]
    tpos = [np.array(t.get_position()) for t in txts]


    # center the text of the 0th cell between the two merged cells
    ##trans = (tpos[1] - tpos[0])/2
    ##if trans[0] > 0 and txts[0].get_ha() == 'right':
    ##    # reduce the transform distance in order to center the text
    ##    trans[0] /= 2
    ##elif trans[0] < 0 and txts[0].get_ha() == 'right':
    ##    # increase the transform distance...
    ##    trans[0] *= 2
    ##
    ##txts[0].set_transform(mpl.transforms.Affine2D().translate(*trans))
    tmp = table.properties()["celld"]
    for i in range(len(ix_list)-1):
        tmp[tuple(ix_list[i])]._loc = 'left'
        table._cells[tuple(ix_list[i+1])].set_facecolor([0,0,0,0])

        # hide the text in the 1st cell
        txts[i+1].set_visible(False)
